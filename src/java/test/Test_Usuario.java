/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.text.DecimalFormat;
import nebuleuse.ORM.Nexo;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.SerializacionXml;
import nebuleuse.seguridad.Autentificacion;
import py.com.base.aplicacion.proyectovigente.ProyectoVigente;
import py.com.base.aplicacion.proyectovigente.ProyectoVigenteExt;
import py.com.base.aplicacion.proyectovigente.ProyectoVigenteSQL;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumento;
import py.com.base.sistema.selector.SelectorUI;
import py.com.base.sistema.usuario.Usuario;
import py.com.base.sistema.usuario.UsuarioDAO;
import py.com.base.vacc.registrador.Registrador;





public class Test_Usuario 
{ 
    public static void main(String[] args) throws IOException, Exception 
    { 
        Autentificacion autorizacion = new Autentificacion();
        Persistencia persistencia = new Persistencia();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        Registrador com = new Registrador();       
        
        //String json = "{\"cuenta\":\"6618104\",\"clave\":\"cird2022\"}";
        String json = "{\"cuenta\":\"vac\",\"clave\":\"vac\"}";
                                                

               Usuario usu = new UsuarioDAO().setJson(json);
                                               
                usu  = new UsuarioDAO().login(
                            usu.getCuenta(), 
                            usu.getClave()
                            );
                
                autorizacion.newTokken(
                        usu.getUsuario().toString(), 
                        usu.getCuenta().trim()
                ) ;                
                
                String t = autorizacion.encriptar();
                
                System.out.println(t);
                
                System.out.println(autorizacion.desencriptar(t));
                
                
    }
}






