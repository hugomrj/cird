/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import py.com.base.aplicacion.estadoproyecto.EstadoProyecto;
import py.com.base.aplicacion.estadoproyecto.EstadoProyectoDAO;
import py.com.base.aplicacion.proyectovigente.ProyectoVigente;
import py.com.base.aplicacion.proyectovigente.ProyectoVigenteDAO;
import py.com.base.aplicacion.proyectovigente.ProyectoVigenteJSON;
import py.com.base.aplicacion.tipoproyecto.TipoProyecto;
import py.com.base.aplicacion.tipoproyecto.TipoProyectoDAO;
import py.com.base.sistema.rol.Rol;
import py.com.base.sistema.rol.RolDAO;





public class Conexion_Test 
{ 
    public static void main(String[] args) throws IOException, Exception 
    { 

        //String json = "{\"cuenta\":\"root\",\"clave\":\"root\"}";
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        
        
                
                EstadoProyectoDAO dao = new EstadoProyectoDAO();                
                List<EstadoProyecto> lista = dao.all();                
                String json = gson.toJson( lista );         
        
                System.out.println(json);

    }
}




