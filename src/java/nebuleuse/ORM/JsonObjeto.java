/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.ORM;


//import static com.google.common.io.Files.map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;



public class JsonObjeto  {

    private Persistencia persistencia = new Persistencia();      
    
    
    public JsonObjeto ( ) throws IOException  {
    
    }
      
    
       
    
//  public JsonElement  array_paginacion ( String sql, Integer page) 
    public JsonObject  json_paginacion ( String sql, Integer page) 
            throws IOException, SQLException, Exception {
        
            Integer total_registro = 0;
            total_registro = new ResultadoSet().total_registros(sql);

            
            JsonObject jsonObjectPaginacion = new JsonObject();
                                    
            
             jsonObjectPaginacion.addProperty("pagina", page);
             jsonObjectPaginacion.addProperty("total_registros", total_registro);
            
            return jsonObjectPaginacion ;         


    }      
    
    
    
    public JsonArray  array_datos (  ResultSet rsData  )  {
    
        JsonArray jsonArray = new JsonArray();
        
        Map<String, String> map = null;
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new Gson();           
        
        
        try {
            
    
            //JsonArray jsonarrayData = new JsonArray();
            while(rsData.next())
            {  
                
                map = registoMap.convertirHashMap(rsData);
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonArray.add( element );
                
            }
            
            
              
        } 
        catch (SQLException ex) {            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally{
            return jsonArray ;  
        }
    }



    
    
//  public JsonElement  array_paginacion ( String sql, Integer page) 
    public JsonObject  json_suma ( ResultSet resultset  ) 
            throws IOException, SQLException, Exception {
        

        JsonObject jsonObject = new JsonObject();
                         
        
        if (resultset.next()){

            ResultSetMetaData metaData = resultset.getMetaData();
            int columnCount = metaData.getColumnCount();        

            for (int i = 1; i < columnCount + 1; i++ ) 
            {  
                String strColumna = metaData.getColumnName(i);                
                jsonObject.addProperty(strColumna, resultset.getString(strColumna) );

            }
        }
            
        return jsonObject ;    

    }      
        
    
        
}
