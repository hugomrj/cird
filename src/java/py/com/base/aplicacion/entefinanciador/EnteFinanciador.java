/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.entefinanciador;

/**
 *
 * @author hugom_000
 */
public class EnteFinanciador {
    
    private Integer ente_financiador;    
    private String nombre;

    public Integer getEnte_financiador() {
        return ente_financiador;
    }

    public void setEnte_financiador(Integer ente_financiador) {
        this.ente_financiador = ente_financiador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    
    
}

