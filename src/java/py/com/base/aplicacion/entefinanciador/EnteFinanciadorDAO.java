/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.entefinanciador;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class EnteFinanciadorDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public EnteFinanciadorDAO ( ) throws IOException  {
    }
      
    
    
    

    public List<EnteFinanciador>  all () {
                
        List<EnteFinanciador>  lista = null;        
        try {                        
                        
            EnteFinanciadorRS rs = new EnteFinanciadorRS();            
            lista = new Coleccion<EnteFinanciador>().resultsetToList(
                    new EnteFinanciador(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
