/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.estadoproyecto;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class EstadoProyectoDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public EstadoProyectoDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<EstadoProyecto>  all () {
                
        List<EstadoProyecto>  lista = null;        
        try {                        
                        
            EstadoProyectoRS rs = new EstadoProyectoRS();            
            lista = new Coleccion<EstadoProyecto>().resultsetToList(
                    new EstadoProyecto(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
