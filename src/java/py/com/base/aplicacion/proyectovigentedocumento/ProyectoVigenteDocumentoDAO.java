/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigentedocumento;


import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.Global;



/**
 *
 * @author hugom_000
 */

public class ProyectoVigenteDocumentoDAO  {

    
    
    public ProyectoVigenteDocumentoDAO () throws IOException  {
    }
    
    


    public Integer insert ( String file_name, String file_id, Integer proyecto, Integer campo_numero ) 
            throws SQLException {

        
        Integer intID = 0;

        
        String strSQL =
                " INSERT INTO aplicacion.proyectos_vigentes_documentos(\n" +
                " file_name, file_id, proyectoid, campo_numero )\n" +
                " VALUES ( '" + file_name + "', " +
                " '" + file_id + "', " +
                " "+  proyecto +", " +
                " " + campo_numero + " )\n" +
                " RETURNING id ;  ";
        

System.out.println(strSQL);        
        
        
        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );

        
        return intID;

    }
    
    
    
    

    public void delete ( Integer proyecto, Integer campo_numero ) 
            throws SQLException {
        
        Integer intID = 0;
        
        String strSQL = "   DELETE\n" +
                        "   FROM aplicacion.proyectos_vigentes_documentos\n" +
                        "   where proyectoid = " + proyecto +
                        "   and campo_numero  = " + campo_numero ;
        
        
        /*
        strSQL =
                " DELETE\n" +
                " FROM aplicacion.proyectos_vigentes_documentos\n" +
                " where proyectoid = " + proyecto;
        */
        
        Persistencia persinstencia = new Persistencia();
        
        persinstencia.ejecutarSQL(strSQL);


    }
    



    public ProyectoVigenteDocumento filterArchivo ( Integer proyectoid, Integer campo_numero ) 
            throws SQLException, Exception {

        
        ProyectoVigenteDocumento obj  = new ProyectoVigenteDocumento();

        
        String strSQL =
                " SELECT id, file_name, file_id, proyectoid, campo_numero\n" +
                " FROM aplicacion.proyectos_vigentes_documentos\n" +
                " WHERE proyectoid =  " + proyectoid +
                " AND campo_numero =  " + campo_numero;
                
        Persistencia persinstencia = new Persistencia();        
        obj = (ProyectoVigenteDocumento) persinstencia.sqlToObject(strSQL, obj);
        
        return obj;

    }
    
    


    public void deleteFile ( Integer proyecto, Integer campo_numero ) 
            throws SQLException {
        
        try {
            
            
            ProyectoVigenteDocumento obj = this.filterArchivo(proyecto, campo_numero);            
            
            if (obj != null)
            {   
                String dir_files = new Global().getValue("dir_files");            
                
                File file = new File( dir_files + obj.getFile_id());                        
                
                if (file.exists()) {                                                         
                    file.delete();                    
                }
            }
        }         
        catch (Exception ex) 
        {
            System.err.println(ex.getMessage());
        }

    }
    
    
    
    // falta una una funcion de lista obtener lista
    
    
    
          
        
}
