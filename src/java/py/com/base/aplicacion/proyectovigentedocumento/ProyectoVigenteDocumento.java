/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigentedocumento;

/**
 *
 * @author hugo
 */
public class ProyectoVigenteDocumento {
    
    private Integer id;
    private String file_name;
    private String file_id;
    private Integer proyectoid;
    private Integer campo_numero;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    public Integer getProyectoid() {
        return proyectoid;
    }

    public void setProyectoid(Integer proyectoid) {
        this.proyectoid = proyectoid;
    }

    public Integer getCampo_numero() {
        return campo_numero;
    }

    public void setCampo_numero(Integer campo_numero) {
        this.campo_numero = campo_numero;
    }


    
    
}

