/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.proyectovigente;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import java.io.File;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.FileXlsx;



/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("proyectosvigentes/xlsx")




public class ProyectoVigenteWSxlsx {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    String json = "";    
    ProyectoVigente com = new ProyectoVigente();       
        
                         
    public ProyectoVigenteWSxlsx() {
        
    }

    
    




  
  
    @GET
    @Path("/consulta1")
    @Produces("aapplication/vnd.ms-excel")
    public Response consulta1(
            @HeaderParam("token") String strToken,            
            @MatrixParam("q") String q,            
            @MatrixParam("z") String z,
            @MatrixParam("ente") Integer ente,
            @MatrixParam("fe1") String fe1, @MatrixParam("fe2") String fe2,
            @MatrixParam("moneda") Integer moneda,
            @MatrixParam("cordinador") Integer cordinador,
            @MatrixParam("tipo") Integer tipo,
            @MatrixParam("estado") Integer estado 
            ) {
 
        
        String path = "";

        if (q == null) { q = ""; }                        

        if (ente == null) { ente = 0; }

        if (fe1 == null) { fe1 = ""; }
        if (fe2 == null) { fe2 = ""; }

        if (moneda == null) { moneda = 0; }
        if (cordinador == null) { cordinador = 0; }
        if (tipo == null) { tipo = 0; }        
        if (estado == null) { estado = 0; }        


        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                  

                
                FileXlsx filexlsx = new FileXlsx();                
                filexlsx.iniciar();
                filexlsx.folder = "/files";                
                filexlsx.name = "/base.xlsx";      
                filexlsx.newlibro();
                
                                
                ProyectoVigenteXLSX1 xlsx1 = new ProyectoVigenteXLSX1();                
                //xlsx1.gen(filexlsx);
                
                
                if (z == null) {        
                    xlsx1.gen(filexlsx, q);
                }   
                else{                    
                    
                    xlsx1.gen_filtro(filexlsx, ente,
                            fe1, fe2, moneda, cordinador, tipo,
                            estado);
                    //xlsx1.gen(filexlsx);
                }
                
                
                filexlsx.newFileStream();
                path = filexlsx.getFilePath();
        
                File file = new File(path);

                ResponseBuilder response = Response.ok((Object) file);
                response.header("Content-Disposition", "attachment; filename=\"test_excel_file.xlsx\"");
                response.header("token", autorizacion.encriptar());

                return response.build();                
                
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")   
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }  




        
        

    }
  
  

  

    
}



































