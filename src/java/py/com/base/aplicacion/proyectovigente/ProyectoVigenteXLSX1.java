/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.base.aplicacion.proyectovigente;

import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.util.FileXlsx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author hugo
 */
public class ProyectoVigenteXLSX1 {
    
    
    public  void gen ( FileXlsx filexlsx, String q )  {    
           
        try {
            
            
            String sql = new ProyectoVigenteSQL().lista(q);   

            ResultSet resultset = new ResultadoSet().resultset(sql);                         
            
            this.hoja01(filexlsx, resultset);
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
    
    
    public  void gen_filtro ( FileXlsx filexlsx,     
            Integer entefinanciador,
            String fecha1, String fecha2,
            Integer moneda,
            Integer cordinador,
            Integer tipoproyecto,
            Integer estadoproyecto
    )    {    
           
        try {
            
            
            //String sql  = new ProyectoVigenteSQL().
            String sql  = "";
                    
            ProyectoVigenteSQL proySQL = new ProyectoVigenteSQL();

            sql = proySQL.lista_filtro(entefinanciador, fecha1, fecha2, moneda, 
                    cordinador, tipoproyecto, estadoproyecto);

            ResultSet resultset = new ResultadoSet().resultset(sql);                         
            
            this.hoja01(filexlsx, resultset);
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );       
            filexlsx.getLibro().write(file);
            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();            
        }
    }    
      
        
    
    
    public  FileXlsx hoja01 ( FileXlsx filexlsx, ResultSet resultset ) 
            throws SQLException, Exception {
        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("id");
            cabecera.add("proyecto_numero");
            cabecera.add("proyecto_nombre");
            cabecera.add("ente_financiador");
            cabecera.add("convenio_contrato_numero");
            cabecera.add("fecha_inicio");
            cabecera.add("fecha_finalizacion");
            cabecera.add("fecha_adenda");
            cabecera.add("observacion");
            cabecera.add("meses_duracion");
            cabecera.add("moneda");
            cabecera.add("monto_moneda_original");
            cabecera.add("monto_guaranies");
            cabecera.add("cordinador");
            cabecera.add("tipo");
            cabecera.add("estado");
            
            filexlsx.setCabecera(cabecera);
            ArrayList<String> campos = new ArrayList<String>();
            
            
            campos.add("id");
            campos.add("proyecto_numero");            
            campos.add("proyecto_nombre");            
            campos.add("xente_financiador");            
            campos.add("convenio_contrato_numero");            
            campos.add("fecha_inicio");            
            campos.add("fecha_finalizacion");            
            campos.add("fecha_adenda");            
            campos.add("observacion");            
            campos.add("meses_duracion");            
            campos.add("xmoneda");            
            campos.add("monto_moneda_original");            
            campos.add("monto_guaranies");            
            campos.add("xcordinador");            
            campos.add("xtipoproyecto");            
            campos.add("xestadoproyecto");            
            
            filexlsx.setCampos(campos);                                
            
           
            filexlsx.newhoja("hoja1");
            filexlsx.writeCabecera(0);     
                        
            filexlsx.writeContenido(resultset);           
            
            this.formato(filexlsx);
        
            return filexlsx;                                                              
            
    }
     
    

    
    public  void formato ( FileXlsx filexlsx  ) {        
        
        //this.newfila(0);
        

        int i = 0;
        for (String titulo : filexlsx.getCabecera()) {     
                       
            
            Cell cell = filexlsx.getHoja().getRow(0).getCell(i);
            
            // formato             
            //System.out.println(this.hoja.getRow(0).getCell(i+1).getCellType());
            
            XSSFCellStyle cellStyle = filexlsx.getHoja().getWorkbook().createCellStyle();
            
            
            java.awt.Color color = new java.awt.Color(220, 220, 220);            
            cellStyle.setFillForegroundColor(new XSSFColor(color, new DefaultIndexedColorMap()));
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);                         
            cell.setCellStyle(cellStyle);
            
            
            i++;
        }
        
        
        
        
        
        
    }            


        
        
    
}
