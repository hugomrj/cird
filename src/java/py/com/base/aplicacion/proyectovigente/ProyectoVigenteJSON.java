/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigente;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import nebuleuse.ORM.JsonObjeto;
import nebuleuse.ORM.RegistroMap;
import nebuleuse.ORM.ResultadoSet;




public class ProyectoVigenteJSON  {


    
    
    public ProyectoVigenteJSON ( ) throws IOException  {
    
    }
      
        
    

    public JsonObject  lista ( Integer page, String buscar) throws IOException, Exception {
        
        Map<String, String> map = null;        
        RegistroMap registoMap = new RegistroMap();                     
        
        
        JsonObject jsonObject = new JsonObject();        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
  

            //String sql = SentenciaSQL.select(new ProyectoVigente());      
            String sql = "";      
            
            
            ProyectoVigenteSQL objSQL = new ProyectoVigenteSQL();               
            sql = objSQL.lista(buscar);
            
            
System.out.println(sql);
            
            
            ResultSet rsData = new ResultadoSet().resultset(sql, page);                
            
            
            ProyectoVigenteDAO dao = new ProyectoVigenteDAO();                
            List<ProyectoVigente> lista = dao.list(1, rsData);                

       
        
            
            
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
        
        
System.out.println(jsonArrayDatos.toString());    

            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            
            
            // suma           
            sql = objSQL.lista_suma(buscar);            
            ResultSet rsSummary = new ResultadoSet().resultset(sql); 
        
            JsonArray jsonarraySuma = new JsonArray();     
            while(rsSummary.next()) 
            {  
                map = registoMap.convertirHashMap(rsSummary);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarraySuma.add( element );
            }   
            
            
            
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
            jsonObject.add("summary", jsonarraySuma);            
            
     


            return jsonObject ;         

    }      
    
    
    
        

    public JsonObject  lista_filtro ( Integer page, Integer entefinanciador,
            String fecha1, String fecha2,
            Integer moneda,
            Integer cordinador,
            Integer tipoproyecto,
            Integer estadoproyecto
    ) {
        
        Map<String, String> map = null;        
        RegistroMap registoMap = new RegistroMap();             
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
              
            //String sql = SentenciaSQL.select(new ProyectoVigente());      
            String sql = "";      
            
            
            ProyectoVigenteSQL objSQL = new ProyectoVigenteSQL();               
            sql = objSQL.lista_filtro(
                    entefinanciador,
                    fecha1, fecha2,
                    moneda,
                    cordinador,
                    tipoproyecto,
                    estadoproyecto);
            
            
            ResultSet rsData = new ResultadoSet().resultset(sql, page);                
            
            
            ProyectoVigenteDAO dao = new ProyectoVigenteDAO();                
            List<ProyectoVigente> lista = dao.list(1, rsData);                
            //String json = gson.toJson( lista );                            
            
        
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
        
        
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // suma           
            sql = objSQL.lista_filtro_suma(entefinanciador, fecha1, fecha2, 
                    moneda, cordinador, tipoproyecto, estadoproyecto);
            
            ResultSet rsSummary = new ResultadoSet().resultset(sql); 
        
            JsonArray jsonarraySuma = new JsonArray();     
            while(rsSummary.next()) 
            {  
                map = registoMap.convertirHashMap(rsSummary);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarraySuma.add( element );
            }   
                        
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
            jsonObject.add("summary", jsonarraySuma);            
                 
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        
    
    
    
    
    

    public JsonObject  search ( String query, Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
              

            ResultadoSet resSet = new ResultadoSet();                   
            String sql = new ProyectoVigenteSQL().search(query);          
            

            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
        
        
    
    
        
}
