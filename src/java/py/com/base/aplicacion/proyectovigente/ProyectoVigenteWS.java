/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.proyectovigente;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumentoDAO;



/**
 * REST Web Service
 * @author hugo
 */


@Path("proyectosvigentes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class ProyectoVigenteWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    String json = "";
    
    ProyectoVigente com = new ProyectoVigente();       
                         
    public ProyectoVigenteWS() {
    }

        
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,
            @MatrixParam("q") String q,            
            @MatrixParam("z") String z,
            @MatrixParam("ente") Integer ente,
            @MatrixParam("fe1") String fe1, @MatrixParam("fe2") String fe2,
            @MatrixParam("moneda") Integer moneda,
            @MatrixParam("cordinador") Integer cordinador,
            @MatrixParam("tipo") Integer tipo,
            @MatrixParam("estado") Integer estado
            ) {
        
        
            if (page == null) { page = 1;}

            if (q == null) { q = ""; }                        
            
            if (ente == null) { ente = 0; }
            
            if (fe1 == null) { fe1 = ""; }
            if (fe2 == null) { fe2 = ""; }
            
            if (moneda == null) { moneda = 0; }
            if (cordinador == null) { cordinador = 0; }
            if (tipo == null) { tipo = 0; }
            if (estado == null) { estado = 0; }
            
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                
                JsonObject jsonObject;
                
                if (z == null) {            
                    
                    jsonObject = new ProyectoVigenteJSON().lista(page, q);
                }   
                else{
                    
                    jsonObject = new ProyectoVigenteJSON().lista_filtro(page,
                            ente,
                            fe1, fe2, 
                            moneda, cordinador, tipo,
                            estado
                            );
                }

                
                            
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
      
 
    


 
    @POST
    //@RolesAllowed
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
   
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                ProyectoVigente req = gson.fromJson(json, ProyectoVigente.class);                   
                
                this.com = (ProyectoVigente) persistencia.insert(req);           
                                                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(this.status)
                        .entity( json )
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        
        }     
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
        
    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                 
        
        ProyectoVigenteExt ext = new ProyectoVigenteExt();     
        
        
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                
                ext = (ProyectoVigenteExt) persistencia.filtrarId(ext, id);  
                
                String json = gson.toJson(ext);
                
                if (ext == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )                        
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
        
        

         

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json 
            ) {

        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                ProyectoVigente req = new Gson().fromJson(json, ProyectoVigente.class);                                                                      
                req.setId(id);
                
                this.com = (ProyectoVigente) persistencia.update(req);
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                    
    
        }        
    }    
    
    
    

    
    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;                
                
                
                ProyectoVigenteDocumentoDAO proyVigDao = new ProyectoVigenteDocumentoDAO();  
//proyVigDao.deleteFile(id);
//                proyVigDao.delete(id);                        
                
                 
                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();          
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
      
    
        
    //@Path("/search/{opt : (.*)}") 
    @GET           
    @Path("/search/") 
    public Response search ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
                
        if (page == null) {                
            page = 1;
        }
        if (q == null){            
            q = "";                
        }

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();       
                
                JsonObject jsonObject = new ProyectoVigenteJSON().search(q, page);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();        
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
        
    
    
        
    
    
    
    
    
}