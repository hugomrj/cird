/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigente;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.RegistroMap;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.ORM.postgres.Conexion;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumento;

/**
 *
 * @author hugo
 */

public class ProyectoVigenteExt extends ProyectoVigente{
    
    //private ProyectoVigenteDocumento documento;
    private ArrayList<ProyectoVigenteDocumento> documentos = new ArrayList<ProyectoVigenteDocumento>();;
    
    
    
    public void extender() throws Exception {
        
        Persistencia persistencia = new Persistencia();     
        
        //this.documento = new ProyectoVigenteDocumento();
        
        String strSQL = " SELECT id, file_name, file_id, proyectoid, campo_numero\n" +
                        " FROM aplicacion.proyectos_vigentes_documentos\n" +
                        " where proyectoid = " +this.getId();
        

        
        ResultSet resultset = new ResultadoSet().resultset(strSQL);                         


        List<ProyectoVigenteDocumento> lista = new Coleccion<ProyectoVigenteDocumento>().resultsetToList(
                new ProyectoVigenteDocumento(),
                resultset
        );
        
        
        this.documentos = (ArrayList<ProyectoVigenteDocumento>) lista ;
        
        
/*        this.documentos
        = (ProyectoVigenteDocumento) persistencia.sqlToObject(
        strSQL, this.documento);
  */      
        
        
    }    

    /*
    public ProyectoVigenteDocumento getDocumento() {
        return documento;
    }

    public void setDocumento(ProyectoVigenteDocumento documento) {
        this.documento = documento;
    }
*/

    
    
    

}
