/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigente;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class ProyectoVigenteSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select( new ProyectoVigente(), busqueda );        
        
        return sql ;             
    }        
           
    
    
    
    
    
    public String lista ( String buscar)
            throws Exception {
    
        String sql = "";           
        
        if (buscar != null) 
        {
            buscar = buscar.replace(" ", "%") ;        
        }
                
        
        
        ReaderT reader = new ReaderT("ProyectoVigente");
        reader.fileExt = "lista.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;      
    }


    
    public String lista_suma ( String buscar)
            throws Exception {
    

        String sql = "";                                 
        
        sql = sql + " select sum(monto_guaranies) monto_guaranies , count(*) cantidad   ";
        sql = sql + " from ( ";
        sql = sql + this.lista(buscar);
        sql = sql + " ) as t";
        
        return sql ;         
        
    }    
    
    
    
    
    
    
    public String lista_filtro ( Integer entefinanciador,
            String fecha1, String fecha2,
            Integer moneda,
            Integer cordinador,
            Integer tipoproyecto,
            Integer estadoproyecto
        )
            throws Exception {
    
        
        
        String sql = "";           
        
        String filtroEnte = "";                
        if (entefinanciador != 0) {
            filtroEnte = "  and p.ente_financiador = " + entefinanciador + " " ;                
        }
                
        
        String filtroFecha1 = "";                
        if (fecha1 != "" ) {      
            filtroFecha1 = "  and fecha_inicio >= '"+fecha1+"' " ;                         
        }        
                
        
        String filtroFecha2 = "";                
        if ( fecha2 != "") {      
            filtroFecha2 = "  and fecha_finalizacion <= '"+fecha2+"'  " ;                         
        }        
                
        
        
        String filtroMoneda = "";                
        if (moneda != 0) {                    
            filtroMoneda = "  and p.moneda =  " + moneda + " " ;
        }

        
        String filtroCordinador = "";                
        if (cordinador != 0) {                               
            filtroCordinador = " and p.cordinador = " + cordinador + " " ;
        }        

        
        String filtroTipoproyecto = "";                
        if (tipoproyecto != 0) {                                         
            filtroTipoproyecto = " and p.tipo_proyecto = " + tipoproyecto + " " ;
        }        
        
        
        String filtroEstadoproyecto = "";                
        if (estadoproyecto != 0) {                                         
            filtroTipoproyecto = " and p.estado_proyecto = " + estadoproyecto + " " ;
        }        
        
                
        
        ReaderT reader = new ReaderT("ProyectoVigente");
        reader.fileExt = "lista_filtro.sql";

        
        
        sql = reader.get( 
                filtroEnte, 
                filtroFecha1,  
                filtroFecha2,  
                filtroMoneda,
                filtroCordinador,
                filtroTipoproyecto,
                filtroEstadoproyecto 
                );    
        
        

        
        return sql ;      
    }
    
    
    
    
    
    public String lista_filtro_suma ( Integer entefinanciador,
            String fecha1, String fecha2,
            Integer moneda,
            Integer cordinador,
            Integer tipoproyecto,
            Integer estadoproyecto
        )
            throws Exception {
    
        
        String sql = "";                                 
        
        sql = sql + " select sum(monto_guaranies) monto_guaranies , count(*) cantidad  ";
        sql = sql + " from ( ";
        sql = sql + this.lista_filtro(entefinanciador,fecha1, fecha2,
                moneda, cordinador, tipoproyecto, estadoproyecto);
        sql = sql + " ) as t";
        
        return sql ;          
        
        
    }
    
    
        
    
}
