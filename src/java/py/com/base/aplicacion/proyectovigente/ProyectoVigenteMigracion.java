package py.com.base.aplicacion.proyectovigente;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import nebuleuse.util.Recurso;


//import org.apache.log4j.Logger;

@WebServlet(
        name = "ProyectoVigenteMigracion", 
        urlPatterns = {"/ProyectoVigente/Migracion"}
)

public class ProyectoVigenteMigracion extends HttpServlet {
    
    static final int BUFFER_SIZE = 1024;
    
     
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
         

        Recurso path = new Recurso();
        //path.pathWebInf("files");
        String nombre_archivo = "";      
        //String save_dir = path.path;

        path.path = "/home/host/archivos/";  
              
        
        
        Enumeration<String> names = request.getHeaderNames();
        while (names.hasMoreElements()) {
            String headerName = names.nextElement();                              
            if (headerName.equals("nombre_archivo")){
                nombre_archivo = request.getHeader(headerName);
            }            
        }
        String pathfile = path.path + nombre_archivo  ;
                     
        

        /*
        long now = System.currentTimeMillis();
        nombre_archivo = String.valueOf(now);
        String pathfile = path.path + nombre_archivo + ".txt" ;
          */        
              
     

        
        ProyectoVigenteFile ccfile = new ProyectoVigenteFile();
        InputStream inputStream = request.getInputStream();
        
        ccfile.crear(inputStream, pathfile);


        // guardar en tabla proyectos vigentes, el nombre del archivo para saber que existe
        
        
        
        
    }
}
