/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigente;

import java.util.Date;
import py.com.base.aplicacion.cordinador.Cordinador;
import py.com.base.aplicacion.entefinanciador.EnteFinanciador;
import py.com.base.aplicacion.estadoproyecto.EstadoProyecto;
import py.com.base.aplicacion.moneda.Moneda;
import py.com.base.aplicacion.tipoproyecto.TipoProyecto;



public class ProyectoVigente {

    public Double getMonto_moneda_original() {
        return monto_moneda_original;
    }

    public void setMonto_moneda_original(Double monto_moneda_original) {
        this.monto_moneda_original = monto_moneda_original;
    }

    public EstadoProyecto getEstado_proyecto() {
        return estado_proyecto;
    }

    public void setEstado_proyecto(EstadoProyecto estado_proyecto) {
        this.estado_proyecto = estado_proyecto;
    }


    private Integer id;    
    private String proyecto_numero;
    private String proyecto_nombre;
    private EnteFinanciador ente_financiador;
    private String convenio_contrato_numero;
    private Date fecha_inicio;
    private Date fecha_finalizacion;
    private Date fecha_adenda;
    private String observacion;
    private Integer meses_duracion;
    private Moneda moneda;
    private Double monto_moneda_original;
    private Date fecha_tipo_cambio;
    private Long tipo_cambio_guaranies;
    private Long monto_guaranies;
    
    private Long monto_adenda_moneda_original;
    
    private Long monto_guaranies_adenda;
    private Long tipo_cambio_adenda;
    
    private Long total_moneda_original;
    private Long total_monto_guaranies;
    
    private Long importe_ejecucion;
    private Long saldo;
    
    private String objetivos_resultados;
    private Cordinador cordinador;
    private TipoProyecto tipo_proyecto;
    private EstadoProyecto estado_proyecto;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getProyecto_nombre() {
        return proyecto_nombre;
    }

    public void setProyecto_nombre(String proyecto_nombre) {
        this.proyecto_nombre = proyecto_nombre;
    }

    public EnteFinanciador getEnte_financiador() {
        return ente_financiador;
    }

    public void setEnte_financiador(EnteFinanciador ente_financiador) {
        this.ente_financiador = ente_financiador;
    }

    public String getConvenio_contrato_numero() {
        return convenio_contrato_numero;
    }

    public void setConvenio_contrato_numero(String convenio_contrato_numero) {
        this.convenio_contrato_numero = convenio_contrato_numero;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_finalizacion() {
        return fecha_finalizacion;
    }

    public void setFecha_finalizacion(Date fecha_finalizacion) {
        this.fecha_finalizacion = fecha_finalizacion;
    }

    public Integer getMeses_duracion() {
        return meses_duracion;
    }

    public void setMeses_duracion(Integer meses_duracion) {
        this.meses_duracion = meses_duracion;
    }


    public Date getFecha_tipo_cambio() {
        return fecha_tipo_cambio;
    }

    public void setFecha_tipo_cambio(Date fecha_tipo_cambio) {
        this.fecha_tipo_cambio = fecha_tipo_cambio;
    }

    public Long getTipo_cambio_guaranies() {
        return tipo_cambio_guaranies;
    }

    public void setTipo_cambio_guaranies(Long tipo_cambio_guaranies) {
        this.tipo_cambio_guaranies = tipo_cambio_guaranies;
    }

    public Long getMonto_guaranies() {
        return monto_guaranies;
    }

    public void setMonto_guaranies(Long monto_guaranies) {
        this.monto_guaranies = monto_guaranies;
    }

    public String getObjetivos_resultados() {
        return objetivos_resultados;
    }

    public void setObjetivos_resultados(String objetivos_resultados) {
        this.objetivos_resultados = objetivos_resultados;
    }

    public Cordinador getCordinador() {
        return cordinador;
    }

    public void setCordinador(Cordinador cordinador) {
        this.cordinador = cordinador;
    }

    public TipoProyecto getTipo_proyecto() {
        return tipo_proyecto;
    }

    public void setTipo_proyecto(TipoProyecto tipo_proyecto) {
        this.tipo_proyecto = tipo_proyecto;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }


    public String getProyecto_numero() {
        return proyecto_numero;
    }

    public void setProyecto_numero(String proyecto_numero) {
        this.proyecto_numero = proyecto_numero;
    }

    public Date getFecha_adenda() {
        return fecha_adenda;
    }

    public void setFecha_adenda(Date fecha_adenda) {
        this.fecha_adenda = fecha_adenda;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }



    public Long getTipo_cambio_adenda() {
        return tipo_cambio_adenda;
    }

    public void setTipo_cambio_adenda(Long tipo_cambio_adenda) {
        this.tipo_cambio_adenda = tipo_cambio_adenda;
    }

    public Long getImporte_ejecucion() {
        return importe_ejecucion;
    }

    public void setImporte_ejecucion(Long importe_ejecucion) {
        this.importe_ejecucion = importe_ejecucion;
    }

    public Long getSaldo() {
        return saldo;
    }

    public void setSaldo(Long saldo) {
        this.saldo = saldo;
    }

    public Long getMonto_adenda_moneda_original() {
        return monto_adenda_moneda_original;
    }

    public void setMonto_adenda_moneda_original(Long monto_adenda_moneda_original) {
        this.monto_adenda_moneda_original = monto_adenda_moneda_original;
    }

    public Long getMonto_guaranies_adenda() {
        return monto_guaranies_adenda;
    }

    public void setMonto_guaranies_adenda(Long monto_guaranies_adenda) {
        this.monto_guaranies_adenda = monto_guaranies_adenda;
    }

    public Long getTotal_moneda_original() {
        return total_moneda_original;
    }

    public void setTotal_moneda_original(Long total_moneda_original) {
        this.total_moneda_original = total_moneda_original;
    }

    public Long getTotal_monto_guaranies() {
        return total_monto_guaranies;
    }

    public void setTotal_monto_guaranies(Long total_monto_guaranies) {
        this.total_monto_guaranies = total_monto_guaranies;
    }
        
    
    
    
    
}
