/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigente;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.xml.Global;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumento;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumentoDAO;


@WebServlet(
        name = "ProyectoVigenteUpload", 
        urlPatterns = { "/ProyectoVigente/Upload" }
)


@MultipartConfig(
  fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
  maxFileSize = 1024 * 1024 * 10,      // 10 MB
  maxRequestSize = 1024 * 1024 * 100   // 100 MB
)


public class ProyectoVigenteUpload extends HttpServlet {

  public void doPost(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
      
            
      try {
                    
          
          Integer proyectoid = 0;
          Integer campo_numero = 0;
          String nombre_archivo = "";
          
          Enumeration<String> names = request.getHeaderNames();
          while (names.hasMoreElements()) {
              String headerName = names.nextElement();

              if (headerName.equals("proyectoid")){
                  proyectoid =  Integer.parseInt(request.getHeader(headerName));
              }
              
              if (headerName.equals("campo_numero")){
                  campo_numero =  Integer.parseInt(request.getHeader(headerName));
              }              
              
              if (headerName.equals("nombre_archivo")){
                  nombre_archivo =  request.getHeader(headerName).trim();
              }                          
              
          }
          
          
          String dir_files = new Global().getValue("dir_files");

          long now = System.currentTimeMillis();
          String file_id = String.valueOf(now);
               
          
          /* Receive file uploaded to the Servlet from the HTML5 form */
          /*
          Part filePart = request.getPart("file");
          String fileName = filePart.getSubmittedFileName();
          */
          
          
          // extenxion de archivo
        String exten = "";
        int indicePunto = nombre_archivo.lastIndexOf(".");
        if (!(indicePunto == -1 || indicePunto == nombre_archivo.length() - 1)) {
            exten =  nombre_archivo.substring(indicePunto + 1);
            exten = "." + exten;
        }
        
          
          
          
          
          for (Part part : request.getParts()) {
              //part.write("/home/host/archivos/" + fileName);
              //part.write( dir_files + fileName);
              part.write( dir_files + file_id + exten);      
              
          }
                    
          
          ProyectoVigenteDocumentoDAO proyVigDao = new ProyectoVigenteDocumentoDAO();          

          
          proyVigDao.deleteFile(proyectoid, campo_numero);
          proyVigDao.delete(proyectoid, campo_numero );          
          
          proyVigDao.insert(nombre_archivo, file_id, proyectoid, campo_numero );
          
          //response.getWriter().print("The file uploaded sucessfully.");
          
          
      }  catch (Exception ex) {
          System.out.println(ex.getMessage());
      }
  }

}    
    
    

