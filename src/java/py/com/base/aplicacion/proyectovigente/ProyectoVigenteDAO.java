/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigente;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.Coleccion;



/**
 *
 * @author hugom_000
 */

public class ProyectoVigenteDAO  {

    
    
    public ProyectoVigenteDAO ( ) throws IOException  {
    }
    
    
    
    
    public List<ProyectoVigente>  list ( Integer page, ResultSet resultSet ) {
                
        List<ProyectoVigente>  lista = null;        
    
        
        try {                        
        
            lista = new Coleccion<ProyectoVigente>().resultsetToList(
                        new ProyectoVigente(), 
                        resultSet
                    );                            
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
          
        
}
