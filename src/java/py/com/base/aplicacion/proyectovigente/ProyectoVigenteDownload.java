/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.proyectovigente;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.util.Download;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumento;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumentoDAO;



@WebServlet(
        name = "ProyectoVigenteDownload", 
        urlPatterns = { "/ProyectoVigente/Download" }
)



public class ProyectoVigenteDownload extends HttpServlet {

    

@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    
    try {
        
        //downloadFile(resp,   this.getServletContext().getRealPath("/
                
        String dir_files = new Global().getValue("dir_files");
        
        // obtener RequestHeader
        Integer regid = Integer.parseInt(req.getHeader("regid"));
        
        Integer campo_numero = Integer.parseInt(req.getHeader("campo_numero"));        
        
System.out.println("campo_numero");        
System.out.println(campo_numero);        
        
        
        ProyectoVigenteDocumento doc = new ProyectoVigenteDocumentoDAO().filterArchivo(regid, campo_numero);
        
        // extencion archivo
        String nombre_archivo = doc.getFile_name();
        String exten = "";
        int indicePunto = nombre_archivo.lastIndexOf(".");
        if (!(indicePunto == -1 || indicePunto == nombre_archivo.length() - 1)) {
            exten =  nombre_archivo.substring(indicePunto + 1);
            exten = "." + exten;
        }        
        
System.out.println("exten");
System.out.println(exten);
        
         
        Download down = new Download();
        // verificar si existe archivo
        File archivo = new File(dir_files+doc.getFile_id()+exten);
        if (archivo.exists()) {
            down.downloadFile(resp, dir_files+doc.getFile_id()+exten , doc.getFile_name() );
        } else {
            down.downloadFile(resp, dir_files+doc.getFile_id() , doc.getFile_name() );
        }        
        
        
        
        
        //Download down = new Download();
        down.downloadFile(resp, dir_files+doc.getFile_id()+exten , doc.getFile_name() );
        
        
    } catch (Exception ex) {
        Logger.getLogger(ProyectoVigenteDownload.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    
}    
 
    
    

}    
    
    

