/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cordinador;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CordinadorDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public CordinadorDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Cordinador>  all () {
                
        List<Cordinador>  lista = null;        
        try {                        
                        
            CordinadorRS rs = new CordinadorRS();            
            lista = new Coleccion<Cordinador>().resultsetToList(
                    new Cordinador(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
