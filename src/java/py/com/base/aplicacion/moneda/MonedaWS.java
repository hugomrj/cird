/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.moneda;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;



/**
 * REST Web Service
 * @author hugo
 */


@Path("monedas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class MonedaWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    String json = "";
    
    Moneda com = new Moneda();       
                         
    public MonedaWS() {
    }

    
    
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {
        
        
            if (page == null) {                
                page = 1;
            }

            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                                
                JsonObject jsonObject = new MonedaJSON().lista(page);
            
                            
                //          JsonObject jsonObject = new MonedaJSON().lista(1);
                
                
                /*
                ProductoDAO dao = new ProductoDAO();                 
                List<Producto> lista = dao.list(page);                 
                List<Producto> lista = null; 
                */
                
                //String json = gson.toJson( lista );     

                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        //.header("total_registros", dao.total_registros )
                        .build();                       
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
      
 
    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                
                this.com = (Moneda) persistencia.filtrarId(this.com, id);  
                
                String json = gson.toJson(this.com);
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )                        
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
        
        


 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
   

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                //Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                Moneda req = gson.fromJson(json, Moneda.class);                   
                
                this.com = (Moneda) persistencia.insert(req);           
                                                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(this.status)
                        .entity( json )
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        
        }     
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
        
    
         
         

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json 
            ) {

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
                
                Moneda req = new Gson().fromJson(json, Moneda.class);                                                      
                req.setMoneda(id);
                
                this.com = (Moneda) persistencia.update(req);
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                    
    
        }        
    }    
    
    
    
    
    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;                
                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();          
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
      
    
    
    
    
 
    @GET    
    @Path("/all") 
    public Response all (
        @HeaderParam("token") String strToken
    ) {
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                MonedaDAO dao = new MonedaDAO();                
                List<Moneda> lista = dao.all();                
                String json = gson.toJson( lista );   
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else
            {                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();    
            }        
        }   
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }          

        
    }    
    

    
    
    
    
    

    
}