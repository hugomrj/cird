/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.tipoproyecto;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class TipoProyectoDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public TipoProyectoDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<TipoProyecto>  all () {
                
        List<TipoProyecto>  lista = null;        
        try {                        
                        
            TipoProyectoRS rs = new TipoProyectoRS();            
            lista = new Coleccion<TipoProyecto>().resultsetToList(
                    new TipoProyecto(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
