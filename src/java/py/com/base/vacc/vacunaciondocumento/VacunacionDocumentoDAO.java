/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunaciondocumento;


import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.Global;



/**
 *
 * @author hugom_000
 */

public class VacunacionDocumentoDAO  {

    
    
    public VacunacionDocumentoDAO () throws IOException  {
    }
    
    


    public Integer insert ( String file_name, String file_id, Integer vacunacion ) 
            throws SQLException {

        
        Integer intID = 0;

        
        String strSQL =
                " INSERT INTO vacc.vacunaciones_documentos(\n" +
                " file_name, file_id, vacunacionid )\n" +
                " VALUES ( '" + file_name + "', " +
                " '" + file_id + "', " +
                " "+  vacunacion +" )\n" +
                " RETURNING id ;  ";
        

        Persistencia persinstencia = new Persistencia();
        
        intID = persinstencia.ejecutarSQL(strSQL, "id" );

        
        return intID;

    }
    
    
    
    

    public void delete ( Integer vacunacion ) 
            throws SQLException {

        
        Integer intID = 0;
        
        String strSQL =
                " DELETE\n" +
                " FROM vacc.vacunaciones_documentos\n" +
                " where vacunacionid = " + vacunacion;

        Persistencia persinstencia = new Persistencia();
        
        persinstencia.ejecutarSQL(strSQL);


    }
    



    public VacunacionDocumento filterProyecto ( Integer vacunacionid ) 
            throws SQLException, Exception {

        
        VacunacionDocumento obj  = new VacunacionDocumento();

        
        String strSQL =
                " SELECT id, file_name, file_id, vacunacionid\n" +
                " FROM vacc.vacunaciones_documentos\n" +
                " WHERE vacunacionid =  " + vacunacionid;
        
        
        Persistencia persinstencia = new Persistencia();        
        obj = (VacunacionDocumento) persinstencia.sqlToObject(strSQL, obj);
        
        return obj;

    }
    
    


    public void deleteFile ( Integer proyecto ) 
            throws SQLException {
        
        try {
            
            
            VacunacionDocumento obj = this.filterProyecto(proyecto);            
            
            if (obj != null)
            {   
                String dir_files = new Global().getValue("dir_files");            
                
                File file = new File( dir_files + obj.getFile_id());                        
                
                if (file.exists()) {                                                         
                    file.delete();                    
                }
            }
        }         
        catch (Exception ex) 
        {
            System.err.println(ex.getMessage());
        }

    }
    
    
    
    
    
          
        
}
