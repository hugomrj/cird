/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.vacc.departamento;

import test.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.text.DecimalFormat;
import nebuleuse.ORM.Nexo;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.SerializacionXml;
import nebuleuse.seguridad.Autentificacion;
import py.com.base.aplicacion.proyectovigente.ProyectoVigente;
import py.com.base.aplicacion.proyectovigente.ProyectoVigenteExt;
import py.com.base.aplicacion.proyectovigente.ProyectoVigenteSQL;
import py.com.base.aplicacion.proyectovigentedocumento.ProyectoVigenteDocumento;





public class Departamento_Test 
{ 
    public static void main(String[] args) throws IOException, Exception 
    { 
        Persistencia persistencia = new Persistencia();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        Departamento com = new Departamento();       
        
        String json = "{\"departamento\":1,\"nombre\":\"Concepción\"}";

        
        
                Departamento req = gson.fromJson(json, Departamento.class);                   
                
                com = (Departamento) persistencia.insert(req);           
                                                
                if (com == null){
                    System.out.println("dep is null");
                }
                
                json = gson.toJson(com);                        
        
                System.out.println(json);
                
                
    }
}






