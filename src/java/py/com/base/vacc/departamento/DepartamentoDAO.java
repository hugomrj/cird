/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.departamento;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class DepartamentoDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public DepartamentoDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Departamento>  all () {
                
        List<Departamento>  lista = null;        
        try {                        
                        
            DepartamentoRS rs = new DepartamentoRS();            
            lista = new Coleccion<Departamento>().resultsetToList(
                    new Departamento(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
        
    
    
}
