/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.distrito;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */
public class DistritoRS {
    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        
    
    public DistritoRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
    

    
    public ResultSet  all (  ) throws Exception {

        statement = conexion.getConexion().createStatement();      

        String sql = new DistritoSQL().all();

        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                 
            
    }
    
    
    public ResultSet  filtrarDpto ( Integer dpto  ) throws Exception {

        statement = conexion.getConexion().createStatement();      

        String sql = new DistritoSQL().filtrarDpto(dpto);

        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                 
            
    }
          


    
        
}
