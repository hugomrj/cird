/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.distrito;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class DistritoSQL {
    
    
    
    public String all ( )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Distrito");
        reader.fileExt = "all.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
    
    
    
    public String filtrarDpto (Integer departamento )
            throws Exception {
    
        String sql = "";                                 
        
        sql =   " SELECT distrito, nombre, departamento\n" +
                " FROM vacc.distritos\n" +
                " where departamento = " + departamento +
                " order by distrito "  ; 
        
        return sql ;      
    }
         
    
        
        
}
