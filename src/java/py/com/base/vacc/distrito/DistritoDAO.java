/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.distrito;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class DistritoDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public DistritoDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Distrito>  all () {
                
        List<Distrito>  lista = null;        
        try {                        
                        
            DistritoRS rs = new DistritoRS();            
            lista = new Coleccion<Distrito>().resultsetToList(
                    new Distrito(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
    
    public List<Distrito>  list ( Integer page, ResultSet resultSet ) {
                
        List<Distrito>  lista = null;        
    
        
        try {                        
        
            lista = new Coleccion<Distrito>().resultsetToList(
                        new Distrito(), 
                        resultSet
                    );                            
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
             
    
    
    public  List<Distrito> filtrarDpto (Integer departamento) throws Exception {      

          /*
          Distrito distrito = new Distrito();                      
          String sql = (new DistritoSQL().filtrarDpto(departamento)); 
          
          distrito = (Distrito) persistencia.sqlToObject(sql, distrito);          
          return distrito;    
          */
                    
                
        List<Distrito>  lista = null;        
            
        try {                   
            
            DistritoRS resultSet = new DistritoRS();      
            lista = new Coleccion<Distrito>().resultsetToList(
                        new Distrito(), 
                        resultSet.filtrarDpto(departamento)
                    );                            
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }          
          
          
          
         
     }
    
}
