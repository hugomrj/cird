/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunador;


/**
 *
 * @author hugom_000
 */
public class Vacunador {
    
    private Integer vacunador;
    private Integer cedula;
    private String nombre ;    
    
    

    public Integer getVacunador() {
        return vacunador;
    }

    public void setVacunador(Integer vacunador) {
        this.vacunador = vacunador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

   
    
    
}


