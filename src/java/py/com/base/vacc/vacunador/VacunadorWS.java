/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.vacc.vacunador;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;




/**
 * REST Web Service
 * @author hugo
 */


@Path("vacunadores")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class VacunadorWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    String json = "";
    
    Vacunador com = new Vacunador();       
                         
    public VacunadorWS() {
    }

        
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {
        
        
            if (page == null) {                
                page = 1;
            }

            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar(); 
                
                JsonObject jsonObject = new VacunadorJSON().lista(page);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                                             
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")     
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
      
 
    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar(); 
                
                this.com = (Vacunador) persistencia.filtrarId(this.com, id);  
                
                String json = gson.toJson(this.com);
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )   
                        .header("token", autorizacion.encriptar())
                        .entity(json)                        
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")                    
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
        

    
    @GET
    @Path("/cedula/{cedula}")
    public Response getcedula(     
            @HeaderParam("token") String strToken,
            @PathParam ("cedula") Integer cedula ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar(); 
                
                VacunadorDAO dao = new VacunadorDAO();
                
                this.com = (Vacunador) dao.getCedula(cedula);  
                
                String json = gson.toJson(this.com);
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )        
                        .header("token", autorizacion.encriptar())
                        .entity(json)                        
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build(); 
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error") 
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        
        
    }    
      
        
            


 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
   

        try {                    
           
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar(); 
                
                Vacunador req = gson.fromJson(json, Vacunador.class);                   
                
                this.com = (Vacunador) persistencia.insert(req);           
                                                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(this.status)
                        .header("token", autorizacion.encriptar())
                        .entity( json )                        
                        .build();       
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        
        }     
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")   
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
        
    
         
         

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json  ) {

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar(); 
                
                Vacunador req = new Gson().fromJson(json, Vacunador.class);                                                      
                req.setVacunador(id);
                
                this.com = (Vacunador) persistencia.update(req);
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                    
    
        }        
    }    
    
    
    
    
    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar(); 
            
                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;                
                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)              
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)   
                            .header("token", autorizacion.encriptar())
                            .build();          
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())  
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
      

    
    @GET       
    //@Path("/search/{opt : (.*)}") 
    @Path("/search/") 
    public Response search ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
        
        
            if (page == null) {                
                page = 1;
            }
            if (q == null){            
                q = "";                
            }
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar(); 
                
                /*
                SocioDAO dao = new SocioDAO();
                
                List<Socio> lista = dao.search(page, q);
                String json = gson.toJson( lista );     
                */
                
                JsonObject jsonObject = new VacunadorJSON().search(q, page);
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString() )  
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();        
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")          
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
        
    

 
    @GET    
    @Path("/all") 
    public Response all (
        @HeaderParam("token") String strToken
    ) {
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar(); 
                
                VacunadorDAO dao = new VacunadorDAO();                
                List<Vacunador> lista = dao.all();                
                String json = gson.toJson( lista );   
                
                return Response
                        .status(Response.Status.OK)                        
                        .entity(json)              
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();    
            }        
        }   
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }          

        
    }    
    

    
    
    
}