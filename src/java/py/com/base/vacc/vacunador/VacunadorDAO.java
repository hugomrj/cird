/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunador;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class VacunadorDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public VacunadorDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Vacunador>  all () {
                
        List<Vacunador>  lista = null;        
        try {                        
                        
            VacunadorRS rs = new VacunadorRS();            
            lista = new Coleccion<Vacunador>().resultsetToList(
                    new Vacunador(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
      public Vacunador getCedula(Integer cedula) throws Exception {      

          Vacunador vacunador = new Vacunador();                      
          String sql = (new VacunadorSQL().getCedula(cedula)); 
          
          vacunador = (Vacunador) persistencia.sqlToObject(sql, vacunador);          
          return vacunador;          
      }
                 
    
        
    
    
}
