/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunador;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugo
 */
public class VacunadorSQL {
    
    
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select( new Vacunador(), busqueda );        
        
        return sql ;             
    }        
           
    
    
    
    public String all ( )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Vacunador");
        reader.fileExt = "all.sql";
        
        sql = reader.get( );    
        
        return sql ;      
    }
        
    
    
    public String getCedula (Integer cedula )
            throws Exception {
    
        String sql = "";                                 
        
        sql =   " SELECT * " +
                " FROM vacc.vacunadores\n " +
                " WHERE cedula = " + cedula; 
        
        return sql ;      
    }
         
    
    
    
}
