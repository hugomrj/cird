/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.registrador;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class RegistradorDAO {

    private Persistencia persistencia = new Persistencia();      
    
    
    public RegistradorDAO ( ) throws IOException  {
   
    }
      
        
    

    public List<Registrador>  all () {
                
        List<Registrador>  lista = null;        
        try {                        
                        
            RegistradorRS rs = new RegistradorRS();            
            lista = new Coleccion<Registrador>().resultsetToList(
                    new Registrador(),
                    rs.all()
            );                        
                    
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
      public Registrador getCedula(Integer cedula) throws Exception {      

          Registrador vacunador = new Registrador();                      
          String sql = (new RegistradorSQL().getCedula(cedula)); 
          
          vacunador = (Registrador) persistencia.sqlToObject(sql, vacunador);          
          return vacunador;          
      }
                 
    
    
    
      public Registrador getUsuario(Integer usuario) throws Exception {      

          Registrador vacunador = new Registrador();                      
          String sql = (new RegistradorSQL().getUsuario(usuario)); 
          
          vacunador = (Registrador) persistencia.sqlToObject(sql, vacunador);          
          return vacunador;          
      }
                 
    
        
      
      
    
    
}
