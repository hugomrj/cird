/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.registrador;

import py.com.base.sistema.usuario.Usuario;



/**
 *
 * @author hugom_000
 */
public class Registrador {
    
    private Integer registrador;
    private Integer cedula;
    private String nombre ;    
    private String email ;    
    private String habilitado ;    
    private Usuario usuario;

    public Integer getRegistrador() {
        return registrador;
    }

    public void setRegistrador(Integer registrador) {
        this.registrador = registrador;
    }

    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String habilitado) {
        this.habilitado = habilitado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
    
}


