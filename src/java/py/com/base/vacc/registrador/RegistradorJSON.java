/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.registrador;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import nebuleuse.ORM.JsonObjeto;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class RegistradorJSON  {


    
    
    public RegistradorJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();


        
        try 
        {   
  

            ResultadoSet resSet = new ResultadoSet();                   
            String sql = SentenciaSQL.select(new Registrador());     


            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    

    public JsonObject  search ( String query, Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
              

            ResultadoSet resSet = new ResultadoSet();                   
            String sql = new RegistradorSQL().search(query);          
            

            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
        
    
    
    
        
}
