/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;

import java.util.Date;
import py.com.base.vacc.departamento.Departamento;
import py.com.base.vacc.distrito.Distrito;
import py.com.base.vacc.registrador.Registrador;
import py.com.base.vacc.vacunador.Vacunador;



/**
 *
 * @author hugom_000
 */
public class Vacunacion {
    
    private Integer id;    
    private Registrador registrador;    
    private Vacunador vacunador;    
    private Integer cantidad_50_mas;    
    private Integer cantidad_18_49; 
    private Integer cantidad_18_menos; 
    private Integer cantidad_vacunados_total;  
    private Date fecha_reporte;    
    private Departamento departamento;
    private Distrito distrito;
    private String transporte;                   
    private String observacion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Vacunador getVacunador() {
        return vacunador;
    }

    public void setVacunador(Vacunador vacunador) {
        this.vacunador = vacunador;
    }

    public Integer getCantidad_50_mas() {
        return cantidad_50_mas;
    }

    public void setCantidad_50_mas(Integer cantidad_50_mas) {
        this.cantidad_50_mas = cantidad_50_mas;
    }

    public Integer getCantidad_18_49() {
        return cantidad_18_49;
    }

    public void setCantidad_18_49(Integer cantidad_18_49) {
        this.cantidad_18_49 = cantidad_18_49;
    }

    public Integer getCantidad_vacunados_total() {
        return cantidad_vacunados_total;
    }

    public void setCantidad_vacunados_total(Integer cantidad_vacunados_total) {
        this.cantidad_vacunados_total = cantidad_vacunados_total;
    }

    public Registrador getRegistrador() {
        return registrador;
    }

    public void setRegistrador(Registrador registrador) {
        this.registrador = registrador;
    }

    public Integer getCantidad_18_menos() {
        return cantidad_18_menos;
    }

    public void setCantidad_18_menos(Integer cantidad_18_menos) {
        this.cantidad_18_menos = cantidad_18_menos;
    }

    public Date getFecha_reporte() {
        return fecha_reporte;
    }

    public void setFecha_reporte(Date fecha_reporte) {
        this.fecha_reporte = fecha_reporte;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public String getTransporte() {
        return transporte;
    }

    public void setTransporte(String transporte) {
        this.transporte = transporte;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    
}


