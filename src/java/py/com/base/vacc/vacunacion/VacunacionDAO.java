/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.RegistroMap;
import py.com.base.vacc.registrador.RegistradorSQL;



/**
 *
 * @author hugom_000
 */

public class VacunacionDAO  {

    
    
    public VacunacionDAO ( ) throws IOException  {
    }
    
    
     
    
    public List<Vacunacion>  list ( Integer page, ResultSet resultSet ) {
                
        List<Vacunacion>  lista = null;        
    
        
        try {                        
        
            lista = new Coleccion<Vacunacion>().resultsetToList(
                        new Vacunacion(), 
                        resultSet
                    );                            
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      


    


    
        
}
