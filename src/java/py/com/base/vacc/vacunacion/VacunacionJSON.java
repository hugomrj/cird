/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import nebuleuse.ORM.JsonObjeto;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.base.vacc.registrador.Registrador;




public class VacunacionJSON  {


    
    
    public VacunacionJSON ( ) throws IOException  {
    
    }
      
        
    

    public JsonObject  lista ( Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
  

            String sql = SentenciaSQL.select(new Vacunacion());                 
            ResultSet rsData = new ResultadoSet().resultset(sql, page);                
            
            
            VacunacionDAO dao = new VacunacionDAO();                
            List<Vacunacion> lista = dao.list(1, rsData);                
            
        
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
        
        
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
                        
            
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    


    public JsonObject  lista ( Integer page, Registrador registrador) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
  

            String sql = SentenciaSQL.select(new Vacunacion());        
            
            if (registrador != null){
                sql += " where registrador =  " +registrador.getRegistrador() ;
            }
            else{
                sql += " where registrador = 0 "  ;
            }


            
            ResultSet rsData = new ResultadoSet().resultset(sql, page);                
            
            
            VacunacionDAO dao = new VacunacionDAO();                
            List<Vacunacion> lista = dao.list(1, rsData);                
            
        
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
        
        
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
                        
            
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
        

    public JsonObject  consulta1 ( Integer page,
            Integer dpt, Integer dis, 
            String fe1, String fe2,
            Integer reg, Integer vac) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
            
            VacunacionSQL vacunacionsql = new VacunacionSQL();
          
            
            //String sql = SentenciaSQL.select(new Vacunacion());  
            String sql = vacunacionsql.consulta1(
                  dpt, dis, fe1, fe2, reg, vac ) ;  
            
            
            ResultSet rsData = new ResultadoSet().resultset(sql, page);                
            
            
            VacunacionDAO dao = new VacunacionDAO();                
            List<Vacunacion> lista = dao.list(1, rsData);                
            
        
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
        
        
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            
            
            // sumas            
            
            sql = vacunacionsql.consulta1_suma(
                  dpt, dis, fe1, fe2, reg, vac ) ;   
            ResultSet rsSuma = new ResultadoSet().resultset(sql);   
            
            JsonObject jsonSuma = new JsonObject();
            jsonSuma = new JsonObjeto().json_suma( rsSuma );
            
            
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
                        
            jsonObject.add("summary", jsonSuma);               
            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    

    
    


    public JsonObject  consulta2 ( Integer page,             
            String fe1, String fe2,
            Integer reg ) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
            
            VacunacionSQL vacunacionsql = new VacunacionSQL();
          
            
            //String sql = SentenciaSQL.select(new Vacunacion());  
            String sql = vacunacionsql.consulta2(
                  fe1, fe2, reg ) ;  
            

            ResultSet rsData = new ResultadoSet().resultset(sql, page);                
            
            /*
            VacunacionDAO dao = new VacunacionDAO();                
            List<Vacunacion> lista = dao.list(1, rsData);                
            */
        
            //datos
            /*
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse( rsData );        
            */
            
            JsonArray jsonArrayDatos = new JsonArray();
            jsonArrayDatos = new JsonObjeto().array_datos(rsData);            
            
        
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonArrayDatos);    
                        
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    

    
    
    
        
}
