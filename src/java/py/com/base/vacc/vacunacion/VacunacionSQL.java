/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;

import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugo
 */
public class VacunacionSQL {
    
        
    
    public String consulta1 ( Integer dpt, Integer dis,
            String fe1, String fe2,
            Integer reg, Integer vac)
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Vacunacion");
        reader.fileExt = "consulta1.sql";
        
        
        String CONdpt = "";
        if ( dpt == 0 ){
            CONdpt = " > 0 "; 
        }
        else {
            CONdpt = " = " + dpt + " "; 
        }
        
        
        String CONdis = "";
        if ( dis == 0){
            CONdis = " > 0 "; 
        }
        else {
            CONdis = " = " + dis + " "; 
        }
        
        
        
        // fechas
        if (fe1.equals("") || fe2.equals("")) {            
            fe1 = "10000101";
            fe2 = "10000102";
        }
        
        
        
        String CONreg = "";
        if ( reg == 0 ){
            CONreg = " > 0 "; 
        }
        else {
            CONreg = " = " + reg + " "; 
        }
        
        
        String CONvac = "";
        if ( vac == 0 ){
            CONvac = " > 0 "; 
        }
        else {
            CONvac = " = " + vac + " "; 
        }
        
        
        sql = reader.get(CONdpt, CONdis, fe1, fe2, CONreg, CONvac);    
        
        return sql ;      
    }
        
    
    


    
    public String consulta1_suma ( Integer dpt, Integer dis,
            String fe1, String fe2,
            Integer reg, Integer vac)
    
            throws Exception {
    
        
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Vacunacion");
        reader.fileExt = "consulta1_suma.sql";
        
        
        String CONdpt = "";
        if ( dpt == 0 ){
            CONdpt = " > 0 "; 
        }
        else {
            CONdpt = " = " + dpt + " "; 
        }
        
        
        String CONdis = "";
        if ( dis == 0){
            CONdis = " > 0 "; 
        }
        else {
            CONdis = " = " + dis + " "; 
        }
        
        
        // fechas
        if (fe1.equals("") || fe2.equals("")) {            
            fe1 = "10000101";
            fe2 = "10000102";
        }
                
        
        
        String CONreg = "";
        if ( reg == 0 ){
            CONreg = " > 0 "; 
        }
        else {
            CONreg = " = " + reg + " "; 
        }
        
        
        String CONvac = "";
        if ( vac == 0 ){
            CONvac = " > 0 "; 
        }
        else {
            CONvac = " = " + vac + " "; 
        }
        
        
        sql = reader.get(CONdpt, CONdis, fe1, fe2, CONreg, CONvac);    
        
        return sql ;      
    }
        
    
    
    public String consulta1_file ( Integer dpt, Integer dis,
            String fe1, String fe2,
            Integer reg, Integer vac)
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Vacunacion");
        reader.fileExt = "consulta1_file.sql";
        
        
        String CONdpt = "";
        if ( dpt == 0 ){
            CONdpt = " > 0 "; 
        }
        else {
            CONdpt = " = " + dpt + " "; 
        }
        
        
        String CONdis = "";
        if ( dis == 0){
            CONdis = " > 0 "; 
        }
        else {
            CONdis = " = " + dis + " "; 
        }
        
        
        
        // fechas
        if (fe1.equals("") || fe2.equals("")) {            
            fe1 = "10000101";
            fe2 = "10000102";
        }
        
        
        
        String CONreg = "";
        if ( reg == 0 ){
            CONreg = " > 0 "; 
        }
        else {
            CONreg = " = " + reg + " "; 
        }
        
        
        String CONvac = "";
        if ( vac == 0 ){
            CONvac = " > 0 "; 
        }
        else {
            CONvac = " = " + vac + " "; 
        }
        
        
        sql = reader.get(CONdpt, CONdis, fe1, fe2, CONreg, CONvac);    
        
        return sql ;      
    }
        
    
    
    
    
    public String consulta2 ( 
            String fe1, String fe2,
            Integer reg )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Vacunacion");
        reader.fileExt = "consulta2.sql";

        
        // fechas
        if (fe1.equals("") || fe2.equals("")) {            
            fe1 = "10000101";
            fe2 = "10000102";
        }
        
        
        
        String CONreg = "";
        if ( reg == 0 ){
            CONreg = " > 0 "; 
        }
        else {
            CONreg = " = " + reg + " "; 
        }
        
        
        
        sql = reader.get(fe1, fe2, CONreg);    
        
        return sql ;      
    }
        
    
    
    public String consulta2_file ( 
            String fe1, String fe2,
            Integer reg )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("Vacunacion");
        reader.fileExt = "consulta2.sql";
        

        
        
        // fechas
        if (fe1.equals("") || fe2.equals("")) {            
            fe1 = "10000101";
            fe2 = "10000102";
        }
        
        
        
        String CONreg = "";
        if ( reg == 0 ){
            CONreg = " > 0 "; 
        }
        else {
            CONreg = " = " + reg + " "; 
        }
        
        sql = reader.get( fe1, fe2, CONreg);    
        
        return sql ;      
    }
        
    
    
        
    
    
    
    
}
