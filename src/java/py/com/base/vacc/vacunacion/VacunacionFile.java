/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;

import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nebuleuse.util.FileXlsx;

/**
 *
 * @author hugo
 */
public class VacunacionFile {
    
    
    
    
    
    public  FileXlsx hoja01 ( FileXlsx filexlsx, ResultSet  resultset ) 
            throws SQLException, Exception {

        

            ArrayList<String> cabecera = new ArrayList<String>();

            cabecera.add("codigo");
            cabecera.add("fecha_reporte");
            cabecera.add("registrador_cedula");
            cabecera.add("registrador_nombre");
            cabecera.add("registrador_email");
            cabecera.add("vacunador_cedula");
            cabecera.add("vacunador_nombre");
            cabecera.add("departamento");
            cabecera.add("distrito");
            cabecera.add("cantidad_50_mas");
            cabecera.add("cantidad_18_49");
            cabecera.add("cantidad_18_menos");
            cabecera.add("cantidad_vacunados_total");
            cabecera.add("transporte");
            cabecera.add("observacion");
            filexlsx.setCabecera(cabecera);


            ArrayList<String> campos = new ArrayList<String>();

            campos.add("codigo");
            campos.add("fecha_reporte");
            campos.add("registrador_cedula");
            campos.add("registrador_nombre");
            campos.add("registrador_email");
            campos.add("vacunador_cedula");
            campos.add("vacunador_nombre");
            campos.add("departamento");
            campos.add("distrito");
            campos.add("cantidad_50_mas");
            campos.add("cantidad_18_49");
            campos.add("cantidad_18_menos");
            campos.add("cantidad_vacunados_total");
            campos.add("transporte");
            
            filexlsx.setCampos(campos);                                
           
            filexlsx.newhoja("registrados");
                       
            
            filexlsx.writeCabecera(0);
            
            filexlsx.writeContenido(resultset);           
        
            
            return filexlsx;      
            
    }
        
    

    
        
    
    public  FileXlsx hoja01_consulta2 ( FileXlsx filexlsx, ResultSet  resultset ) 
            throws SQLException, Exception {

        

            ArrayList<String> cabecera = new ArrayList<String>();

            
            cabecera.add("fecha");
            cabecera.add("cedula");
            cabecera.add("nombre");
            cabecera.add("email");
            cabecera.add("registrador");
            filexlsx.setCabecera(cabecera);


            ArrayList<String> campos = new ArrayList<String>();


            campos.add("fecha");
            campos.add("cedula");
            campos.add("nombre");
            campos.add("email");
            campos.add("registrador");

            
            filexlsx.setCampos(campos);                                
           
            filexlsx.newhoja("no registrados");
                       
            
            filexlsx.writeCabecera(0);
            
            filexlsx.writeContenido(resultset);           
        
            
            return filexlsx;      
            
    }
        
    

    
    
    public void gen ( FileXlsx filexlsx, ResultSet  resultset )  {    
    
        
        
        try {

                
            this.hoja01(filexlsx, resultset);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );                

            filexlsx.getLibro().write(file);

            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        
        
        
    }    


    
    
    public void gen_consulta2 ( FileXlsx filexlsx, ResultSet  resultset )  {    
    
        
        
        try {

                
            this.hoja01_consulta2(filexlsx, resultset);
            
            
            String ruta = "";
            ruta = filexlsx.getFilePath();

            FileOutputStream file = new FileOutputStream( ruta );                

            filexlsx.getLibro().write(file);

            file.close();                

            
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        
        
        
    }    

    
    
    
}
