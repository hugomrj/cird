/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.util.Download;
import py.com.base.vacc.vacunaciondocumento.VacunacionDocumento;
import py.com.base.vacc.vacunaciondocumento.VacunacionDocumentoDAO;




@WebServlet(
        name = "VacunacionDownload", 
        urlPatterns = { "/Vacunacion/Download" }
)



public class VacunacionDownload extends HttpServlet {

    

@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
  throws ServletException, IOException {
    
    try {
        
        //downloadFile(resp,   this.getServletContext().getRealPath("/
                
        String dir_files = new Global().getValue("dir_files");
        
        // obtener RequestHeader
        Integer regid = Integer.parseInt(req.getHeader("regid"));
        
        
        VacunacionDocumento doc = new VacunacionDocumentoDAO().filterProyecto(regid);
        
        Download down = new Download();
        down.downloadFile(resp, dir_files+doc.getFile_id(), doc.getFile_name() );
        
        
    } catch (Exception ex) {
        Logger.getLogger(VacunacionDownload.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    
}    
 
    
    

}    
    
    

