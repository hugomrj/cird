/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.sql.SQLException;
import java.util.Enumeration;

import nebuleuse.ORM.xml.Global;
import py.com.base.vacc.vacunaciondocumento.VacunacionDocumentoDAO;



@WebServlet(
        name = "VacunacionUpload", 
        urlPatterns = { "/Vacunacion/Upload" }
)


@MultipartConfig(
  fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
  maxFileSize = 1024 * 1024 * 10,      // 10 MB
  maxRequestSize = 1024 * 1024 * 100   // 100 MB
)


public class VacunacionUpload extends HttpServlet {

  public void doPost(HttpServletRequest request, HttpServletResponse response) 
          throws ServletException, IOException {
      
      
      
      try {
          
          
          
          Integer vacunacionid = 0;
          Enumeration<String> names = request.getHeaderNames();
          while (names.hasMoreElements()) {
              String headerName = names.nextElement();
              if (headerName.equals("vacunacionid")){
                  vacunacionid =  Integer.parseInt(request.getHeader(headerName));
              }
          }
          
          
          String dir_files = new Global().getValue("dir_files");
          
          long now = System.currentTimeMillis();
          String file_id = String.valueOf(now);
                    
          /* Receive file uploaded to the Servlet from the HTML5 form */
          Part filePart = request.getPart("file");
          String fileName = filePart.getSubmittedFileName();
          
          
          
          for (Part part : request.getParts()) {
              //part.write("/home/host/archivos/" + fileName);
              //part.write( dir_files + fileName);
              part.write( dir_files + file_id);              
          }
                    
          
          VacunacionDocumentoDAO vacDocDao = new VacunacionDocumentoDAO();          
          
          vacDocDao.deleteFile(vacunacionid);
          vacDocDao.delete(vacunacionid);          
          vacDocDao.insert(fileName, file_id, vacunacionid);
                              
          //response.getWriter().print("The file uploaded sucessfully.");
          
          
      } 
      catch (SQLException ex) {
          System.out.println(ex.getMessage());
      } catch (Exception ex) {
          System.out.println(ex.getMessage());
      }
  }

}    
    
    

