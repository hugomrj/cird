/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;

import nebuleuse.ORM.Persistencia;
import py.com.base.vacc.vacunaciondocumento.VacunacionDocumento;

/**
 *
 * @author hugo
 */

public class VacunacionExt extends Vacunacion {
    
    private VacunacionDocumento documento;
    
    
    public void extender() throws Exception {
        
        Persistencia persistencia = new Persistencia();     
        
        
        
        this.documento = new VacunacionDocumento();
        
        String strSQL = " SELECT id, file_name, file_id, vacunacionid\n" +
                        " FROM vacc.vacunaciones_documentos\n" +
                        " where vacunacionid = " +this.getId();
        
        this.documento  
                = (VacunacionDocumento) persistencia.sqlToObject(
                        strSQL, this.documento);
        
    }    

    
    public VacunacionDocumento getDocumento() {
        return documento;
    }

    public void setDocumento(VacunacionDocumento documento) {
        this.documento = documento;
    }


    
    
    

}
