/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.vacc.vacunacion;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletContext;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.FileXlsx;

/**
 *
 * @author hugo
 */
@WebServlet(name = "Vacunacion_Consulta1_XLSX", 
        urlPatterns = {"/consulta1.xlsx"})
public class Vacunacion_Consulta1_XLSX extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
             {
    
        //response.setContentType("text/html;charset=UTF-8");
        
        try {         
        
            String strToken = "";
        
            strToken = request.getHeader("token");
            Autentificacion autorizacion = new Autentificacion();
            
            if (autorizacion.verificar(strToken))
            {  
            
                
                    Integer dpt = 0;
                    dpt = Integer.parseInt( request.getParameter("dpt").toString() ) ;       
                    
                    Integer dis = 0;                    
                    dis = Integer.parseInt( request.getParameter("dis").toString() ) ;       
                
                    String fe1 = "";
                    fe1 = request.getParameter("fe1").toString() ;       
                    
                    String fe2 = "";
                    fe2 = request.getParameter("fe2").toString() ;       
                    
                    Integer reg = 0;
                    reg = Integer.parseInt( request.getParameter("reg").toString() ) ;       
                    
                    Integer vac = 0;                    
                    vac = Integer.parseInt( request.getParameter("vac").toString() ) ;       
                                    
                    
                    

                FileXlsx filexlsx = new FileXlsx();
                
                filexlsx.Iniciar(request);
                filexlsx.folder = "/files";                
                filexlsx.name = "/base.xlsx";                
                filexlsx.newlibro();
                
                
                
                
            VacunacionSQL vacunacionsql = new VacunacionSQL();
            
            
            
            /*
            String sql = vacunacionsql.consulta1(
                  dpt, dis, fe1, fe2, reg, vac ) ;  
            */
            
            String sql = vacunacionsql.consulta1_file(
                  dpt, dis, fe1, fe2, reg, vac ) ;              
                        
            
            ResultSet rsData = new ResultadoSet().resultset(sql);                
                           
                                
                
                VacunacionFile vacunacionFile = new VacunacionFile();                
                vacunacionFile.gen(filexlsx, rsData);                
                filexlsx.newFileStream();
                
                
                
                ServletContext context = getServletContext();
                response.setHeader("token", autorizacion.encriptar());
                filexlsx.getServeltFile(request, response, context);
                
                filexlsx.close();
                
                                
            }
            else{   
                //System.out.println("no autorizado");                
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
            
            
        } 
        
        
        
        catch (IOException ex) {
            Logger.getLogger(Vacunacion_Consulta1_XLSX.class.getName()).log(Level.SEVERE, null, ex);
        }  catch (Exception ex) {
            Logger.getLogger(Vacunacion_Consulta1_XLSX.class.getName()).log(Level.SEVERE, null, ex);
        }            
            
                
    }

}
