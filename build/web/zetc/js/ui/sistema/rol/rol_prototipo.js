

function Rol(){
    
   this.tipo = "rol";   
   this.recurso = "roles";   
   this.value = 0;
   this.form_descrip = "rol_descripcion";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/sistema";   
      
   
   
   this.titulosin = "Rol"
   this.tituloplu = "Roles"   
      
   //this.tablalinea=  'rol';
   this.campoid=  'rol';
   this.tablacampos =  ['rol', 'nombre_rol'];
   
    this.etiquetas =  ['Rol', 'descripcion'];                                  
   
   this.tbody_id = "rol-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "rol-acciones";   
      
   //this.tabs =  ['usuario', 'sucursal' ];
   this.tabs =  ['usuario'];
   this.parent = null;
   
}



Rol.prototype.lista_new = function( obj  ) {                

    reflex.form(obj);
    reflex.acciones.button_add(obj);          
    
};



Rol.prototype.form_validar = function() {    
    return true;
};



Rol.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};



Rol.prototype.tabuladores = function() {  
           
        var obj = new Rol();
        coleccion.ini(obj);
        
        document.getElementById( "rol-tabuladores" ).innerHTML =  coleccion.gene();    
    
        var usurol = new UsuarioRol(); 
        var rol = new Rol();
        
        rol.value = document.getElementById('rol_rol').value;          
        usurol.cabecera = rol;                
        usurol.tablacamposoculto = [0,3, 4];    
        
  
                
        
        coleccion.objetos = [ usurol];                        
        coleccion.interaccion();     
        
};







Rol.prototype.linea = function(id) {    
    
        ajax.metodo = "GET";   
        ajax.url = html.url.absolute() +'/api/'+this.recurso+'/'+id ;    
        ajax.metodo = "GET";   
        var json = ajax.private.json();   
        
        return json;          
    
    
};






Rol.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};








