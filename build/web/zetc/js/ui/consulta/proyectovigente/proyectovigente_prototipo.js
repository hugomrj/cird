

function ProyectoVigente(){
    
    this.tipo = "proyectovigente";   
    this.recurso = "proyectosvigentes";   
    this.value = 0;
    this.form_descrip = "nombre";
    this.json_descrip = "nombre";
   
    this.dom="";
    this.carpeta=  "/aplicacion";   
   
   
    this.titulosin = "Proyecto"
    this.tituloplu = "Proyectos"   
      
   
    this.campoid=  'id';
    this.tablacampos =  [ 'proyecto_numero', 'proyecto_nombre', 'ente_financiador.nombre', 
        'convenio_contrato_numero', 'fecha_inicio', 'fecha_finalizacion',
        'meses_duracion', 'moneda.nombre', 'monto_moneda_original', 
        'monto_guaranies', 'cordinador.nombre', 'tipo_proyecto.nombre', 
        'estado_proyecto.nombre'];
   
    this.etiquetas =  [ 'numero', 'proyecto_nombre', 'ente_financiador', 
        'convenio_contrato_numero', 'fecha_inicio', 'fecha_finalizacion',
        'meses_duracion', 'moneda', 'monto_moneda_original',
        'monto_guaranies', 'cordinador', 'tipo_proyecto' , 'estado_proyecto'
    ];                                  
    
    this.tablaformat = ['C', 'C', 'C', 
        'C', 'D', 'D',
        'N', 'C', 'N',
        'N', 'C', 'C',
        'C'];                                  
            
            
    this.tbody_id = "proyectovigente-tb";
      
    this.botones_lista = [ this.lista_new] ;
    this.botones_form = "proyectovigente-acciones";   
         
    this.parent = null;
   
   
    this.combobox = 
            {
                "ente_financiador":{
                   "value":"ente_financiador",
                   "inner":"nombre"
                },                
                "moneda":{                    
                    "value":"moneda",
                    "inner":"nombre"
                },     
                "tipo_proyecto":{
                    "value":"tipo_proyecto",
                    "inner":"nombre"
                },     
                "estado_proyecto":{
                    "value":"estado_proyecto",
                    "inner":"nombre"
                }                
                
            };      
   
   
   this.filtro = "";
   
   
}





ProyectoVigente.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
    // 
    obj.form_ini();
    obj.form_accion();
    
    
    // no mostrar form de archivo
    var form_archivo = document.getElementById('form_archivo');      
    form_archivo.style.display = "none";
    
    document.getElementById("proyectovigente_objetivos_resultados").disabled = false;
    document.getElementById("proyectovigente_objetivos_resultados").value = "";
    
    
};





ProyectoVigente.prototype.form_accion = function() {    

    
    var proyectovigente_cordinador = document.getElementById('proyectovigente_cordinador');            
    proyectovigente_cordinador.onblur  = function() {      
         
         
         proyectovigente_cordinador.value = fmtNum(proyectovigente_cordinador.value);      
         proyectovigente_cordinador.value = NumQP(proyectovigente_cordinador.value);      
    
        var  id = (proyectovigente_cordinador.value );
        
        
        ajax.url = html.url.absolute() +'/api/cordinadores/'+id;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {            
                    
                    var ojson = JSON.parse(xhr.responseText) ;     
                    document.getElementById('cordinador_descripcion').innerHTML = 
                            ojson['nombre']; 
                }
                else{
                    document.getElementById('cordinador_descripcion').innerHTML = "";
                }                   
                

            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
         
    
     };      
    proyectovigente_cordinador.onblur();          
    
    
    
    
    
    var more_proyectovigente_cordinador = document.getElementById('ico-more-proyectovigente_cordinador');
    more_proyectovigente_cordinador.addEventListener('click',
        function(event) {     

            var obj = new Cordinador();      

            
            obj.acctionresul = function(id) {    
                proyectovigente_cordinador.value = id; 
                proyectovigente_cordinador.onblur(); 
            };       
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);

        },
        false
    );        
    
};






ProyectoVigente.prototype.form_ini = function() {    
    
     
    
    var proyectovigente_meses_duracion = document.getElementById('proyectovigente_meses_duracion');          
    proyectovigente_meses_duracion.onblur  = function() {     
        proyectovigente_meses_duracion.value  = fmtNum(proyectovigente_meses_duracion.value);
    };     
    proyectovigente_meses_duracion.onblur();       
    
    
    
    var proyectovigente_monto_moneda_original = document.getElementById('proyectovigente_monto_moneda_original');          
    proyectovigente_monto_moneda_original.onblur  = function() {                
        proyectovigente_monto_moneda_original.value  = fmtNum(proyectovigente_monto_moneda_original.value);
    };     
    proyectovigente_monto_moneda_original.onblur();       
        
    
    
    
    var proyectovigente_tipo_cambio_guaranies = document.getElementById('proyectovigente_tipo_cambio_guaranies');          
    proyectovigente_tipo_cambio_guaranies.onblur  = function() {                
        proyectovigente_tipo_cambio_guaranies.value  = fmtNum(proyectovigente_tipo_cambio_guaranies.value);
    };     
    proyectovigente_tipo_cambio_guaranies.onblur();       
        
    
    var proyectovigente_monto_guaranies = document.getElementById('proyectovigente_monto_guaranies');          
    proyectovigente_monto_guaranies.onblur  = function() {                
        proyectovigente_monto_guaranies.value  = fmtNum(proyectovigente_monto_guaranies.value);
    };     
    proyectovigente_monto_guaranies.onblur();       
            
    
    
    
    
    // boton enviar
    var btn_enviar = document.getElementById('btn_enviar');
    btn_enviar.onclick = function(event) {     
       

        var proyectoid  = document.getElementById('proyectovigente_id').value;        
        
        var filetxt = document.getElementById("migracion_file").files[0];

        var nombre_archivo = filetxt.name;


        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);



        migracion_proceso_promesa( formdata, nombre_archivo, proyectoid )
            .then(( xhr ) => {
                
                //document.getElementById( "divroot" ).innerHTML =  ""; 
        
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );

            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
        
        
        

    }
          
        
    
    
    
};






ProyectoVigente.prototype.form_validar = function() {    
   
   
    
    var proyectovigente_proyecto_numero = document.getElementById('proyectovigente_proyecto_numero');    
    if (proyectovigente_proyecto_numero.value == "")         
    {
        msg.error.mostrar("Numero de proyecto vacio");           
        proyectovigente_proyecto_numero.focus();
        proyectovigente_proyecto_numero.select();        
        return false;
    }  
   
   
   
    var proyectovigente_proyecto_nombre = document.getElementById('proyectovigente_proyecto_nombre');    
    if (proyectovigente_proyecto_nombre.value == "")         
    {
        msg.error.mostrar("Nombre de proyecto vacio");           
        proyectovigente_proyecto_nombre.focus();
        proyectovigente_proyecto_nombre.select();        
        return false;
    }  
   
   
   
   
    var proyectovigente_convenio_contrato_numero = document.getElementById('proyectovigente_convenio_contrato_numero');    
    if (proyectovigente_convenio_contrato_numero.value == "")         
    {
        msg.error.mostrar("Convenio contrato numero vacio");           
        proyectovigente_convenio_contrato_numero.focus();
        proyectovigente_convenio_contrato_numero.select();        
        return false;
    }  
   
   
   
    var proyectovigente_meses_duracion = document.getElementById('proyectovigente_meses_duracion');    
    if (fmtNum(proyectovigente_meses_duracion.value) == 0)         
    {
        msg.error.mostrar("Duracion meses vacio");           
        proyectovigente_meses_duracion.focus();
        proyectovigente_meses_duracion.select();        
        return false;
    }  
   
   
   
   
   
   
    var proyectovigente_cordinador = document.getElementById('proyectovigente_cordinador');    
    if (fmtNum(proyectovigente_cordinador.value) == 0)         
    {
        msg.error.mostrar("Falta seleccionar cordinador de proyecto");           
        return false;
    }  
   
   
    return true;
};










ProyectoVigente.prototype.main_list = function(obj, page) {    

    if (page === undefined) {    
        page = 1;
    }



    let promesa = arasa.vista.lista_paginacion(obj, page);
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
                        
                    
            // suma                
            var ojson = JSON.parse( xhr.responseText ) ; 
            var sum = ojson['summary'][0]['monto_guaranies']  ;  
            var cant = ojson['summary'][0]['cantidad']  ; 

            if (!(sum === undefined)) {    
                document.getElementById( "monto_guaranies" ).innerHTML =  fmtNum(sum.toString());                
                document.getElementById( "cantidad" ).innerHTML =  fmtNum(cant.toString());                
            }   
            else{
                document.getElementById( "monto_guaranies" ).innerHTML =  fmtNum("0");                
                document.getElementById( "cantidad" ).innerHTML =  fmtNum("0");                
            }



            // cuadro de busqueda            
            fetch(  html.url.absolute() + '/aplicacion/proyectovigente/'+ '/htmf/busqueda_bar.html' )
              .then(response => {
                return response.text();
              })
              .then(data => {
                document.getElementById( "bar_busqueda" ).innerHTML =  data;    
                busqueda_bar_accion(obj);
              })            

    
            acciones_main_list(obj);
    
    
var padre2 = document.getElementById('btn_proyectovigente_nuevo').parentNode;
var padre1 = padre2.parentNode;
padre1.removeChild(padre2);
    
    
    
    
    
    
            
        })
        /*
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 
    */

};









ProyectoVigente.prototype.carga_combos = function( obj  ) {                
    

    var proyectovigente_ente_financiador = document.getElementById("proyectovigente_ente_financiador");
    var idedovalue = proyectovigente_ente_financiador.value;


    var entef = new EnteFinanciador(); 
    entef.combobox("proyectovigente_ente_financiador");    



    var moneda = new Moneda(); 
    moneda.combobox("proyectovigente_moneda");    


    var tipoproyecto = new TipoProyecto(); 
    tipoproyecto.combobox("proyectovigente_tipo_proyecto");            


    var estadoproyecto = new EstadoProyecto(); 
    estadoproyecto.combobox("proyectovigente_estado_proyecto");            



};








ProyectoVigente.prototype.post_form_id = function( obj  ) {                


/*
        document.getElementById( 'btn_proyectovigente_nuevo' ).style.display = "none";   
        document.getElementById( 'btn_proyectovigente_modificar' ).style.display = "none";   
        document.getElementById( 'btn_proyectovigente_eliminar' ).style.display = "none";   
*/

var padre2 = document.getElementById('btn_proyectovigente_nuevo').parentNode;
var padre1 = padre2.parentNode;
padre1.removeChild(padre2);


var padre2 = document.getElementById('btn_proyectovigente_modificar').parentNode;
var padre1 = padre2.parentNode;
padre1.removeChild(padre2);

var padre2 = document.getElementById('btn_proyectovigente_eliminar').parentNode;
var padre1 = padre2.parentNode;
padre1.removeChild(padre2);


document.getElementById( 'form_archivo' ).style.display = "none";   



/*
alert(padre1.innerHTML)
alert(padre2.innerHTML)
*/



/*
let d = document.getElementById("top");
let d_nested = document.getElementById("nested");
let throwawayNode = d.removeChild(d_nested);
*/




    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento']) ;       
    
    if (typeof json === 'undefined') 
    { 
        document.getElementById( 'fileD' ).style.display = "none";   
                //result.innerHTML = "Variable is Undefined"; 
    } 
    else 
    {   
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']

        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/ProyectoVigente/Download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("regid", document.getElementById( 'proyectovigente_id' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();

        }
            

 



    } 
    
    
};




ProyectoVigente.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    //ret = ";ente=5;moneda=0;cordinador=0;tipo=0;z=a";
/*    
    var busquedatexto = document.getElementById("busquedatexto");
    console.log(busquedatexto);

var simple = document.getElementById( "busqueda_simple" )
if ( document.getElementById( "busqueda_simple" )) {
    
      alert( simple.style.display );
}
*/
  


/*
    if (!(busquedatexto === null)) {                
        ret = ";q="+busquedatexto.value;
    }
  */  

    return ret;
    
};











