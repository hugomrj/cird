

function Moneda(){
    
   this.tipo = "moneda";   
   this.recurso = "monedas";   
   this.value = 0;
   this.form_descrip = "moneda_nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Moneda"
   this.tituloplu = "Monedas"   
      
   
   this.campoid=  'moneda';
   this.tablacampos =  ['moneda', 'nombre'];
   
   this.etiquetas =  ['Moneda', 'Nombre'];                                  
    
   this.tablaformat = ['C', 'C'];                                  
   
   this.tbody_id = "moneda-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "moneda-acciones";   
         
   this.parent = null;
   
   
}





Moneda.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





Moneda.prototype.form_ini = function() {    
    
};






Moneda.prototype.form_validar = function() {    
    
   
    var moneda_nombre = document.getElementById('moneda_nombre');    
    if (moneda_nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        moneda_nombre.focus();
        moneda_nombre.select();        
        return false;
    }              
    
    return true;
};










Moneda.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
            
            
//                blista.innerHTML = "fafafafaf";        
                

            

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Moneda.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['moneda'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}


