

function ProyectoVigente(){
    
    this.tipo = "proyectovigente";   
    this.recurso = "proyectosvigentes";   
    this.value = 0;
    this.form_descrip = "nombre";
    this.json_descrip = "nombre";
   
    this.dom="";
    this.carpeta=  "/aplicacion";   
   
   
    this.titulosin = "Proyecto"
    this.tituloplu = "Proyectos"   
      
   
    this.campoid=  'id';
    this.tablacampos =  [ 'proyecto_numero', 'proyecto_nombre', 'ente_financiador.nombre', 
        'convenio_contrato_numero', 'fecha_inicio', 'fecha_finalizacion',
        'fecha_adenda',
        'meses_duracion', 'moneda.nombre', 'total_moneda_original', 
        'monto_guaranies', 'cordinador.nombre', 'tipo_proyecto.nombre', 
        'estado_proyecto.nombre'
    ];
   
    this.etiquetas =  [ 'numero', 'proyecto_nombre', 'ente_financiador', 
        'convenio_contrato_numero', 'fecha_inicio', 'fecha_finalizacion',
        'fecha_adenda',
        'meses_duracion', 'moneda', 'total_moneda_original',
        'monto_guaranies', 'cordinador', 'tipo_proyecto' , 
        'estado_proyecto'
    ];                                  
    
    this.tablaformat = ['C', 'C', 'C', 
        'C', 'D', 'D', 
        'D',
        'N', 'C', 'N',
        'N', 'C', 'C',
        'C'];                                  
            
            
    this.tbody_id = "proyectovigente-tb";
      
    this.botones_lista = [ this.lista_new] ;
    this.botones_form = "proyectovigente-acciones";   
         
    this.parent = null;
   
   
    this.combobox = 
            {
                "ente_financiador":{
                   "value":"ente_financiador",
                   "inner":"nombre"
                },                
                "moneda":{                    
                    "value":"moneda",
                    "inner":"nombre"
                },     
                "tipo_proyecto":{
                    "value":"tipo_proyecto",
                    "inner":"nombre"
                },     
                "estado_proyecto":{
                    "value":"estado_proyecto",
                    "inner":"nombre"
                }        
            };         
   
   this.filtro = "";
   
   
}







ProyectoVigente.prototype.new = function( obj  ) {                


    reflex.form_new( obj ); 
         
    
    reflex.acciones.button_add_promise(obj);        
    
    // 
    obj.form_ini();
    obj.form_accion();
    
    
    // no mostrar form de archivo  
    //form_archivo.style.display = "none";
    
    document.getElementById("proyectovigente_observacion").disabled = false;
    document.getElementById("proyectovigente_observacion").value = "";
    
    
    document.getElementById("proyectovigente_objetivos_resultados").disabled = false;
    document.getElementById("proyectovigente_objetivos_resultados").value = "";
    
    
    
    document.getElementById("archivos_grupo1").style.display = "none";    
    document.getElementById("archivos_grupo2").style.display = "none";    
    
    
    document.getElementById("proyectovigente_monto_guaranies_adenda").disabled = true;
    document.getElementById("proyectovigente_total_moneda_original").disabled = true;
    document.getElementById("proyectovigente_total_monto_guaranies").disabled = true;

    
};





ProyectoVigente.prototype.form_accion = function() {    

    
    var proyectovigente_cordinador = document.getElementById('proyectovigente_cordinador');            
    proyectovigente_cordinador.onblur  = function() {      
         
         
         proyectovigente_cordinador.value = fmtNum(proyectovigente_cordinador.value);      
         proyectovigente_cordinador.value = NumQP(proyectovigente_cordinador.value);      
    
        var  id = (proyectovigente_cordinador.value );
        
        
        ajax.url = html.url.absolute() +'/api/cordinadores/'+id;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {            
                    
                    var ojson = JSON.parse(xhr.responseText) ;     
                    document.getElementById('cordinador_descripcion').innerHTML = 
                            ojson['nombre']; 
                }
                else{
                    document.getElementById('cordinador_descripcion').innerHTML = "";
                }                   
                

            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
         
    
     };      
    proyectovigente_cordinador.onblur();          
    
    
    
    
    
    var more_proyectovigente_cordinador = document.getElementById('ico-more-proyectovigente_cordinador');
    more_proyectovigente_cordinador.addEventListener('click',
        function(event) {     

            var obj = new Cordinador();      

            
            obj.acctionresul = function(id) {    
                proyectovigente_cordinador.value = id; 
                proyectovigente_cordinador.onblur(); 
            };       
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);

        },
        false
    );        
    
};






ProyectoVigente.prototype.form_ini = function() {    
    
     
    
    var proyectovigente_meses_duracion = document.getElementById('proyectovigente_meses_duracion');          
    proyectovigente_meses_duracion.onblur  = function() {     
        proyectovigente_meses_duracion.value  = fmtNum(proyectovigente_meses_duracion.value);
    };     
    proyectovigente_meses_duracion.onblur();       
    
        
    var proyectovigente_monto_moneda_original = document.getElementById('proyectovigente_monto_moneda_original');          
    proyectovigente_monto_moneda_original.onblur  = function() {                
        proyectovigente_monto_moneda_original.value  = fmtNum(proyectovigente_monto_moneda_original.value);
        
        // suma      
         document.getElementById('proyectovigente_total_moneda_original').value = 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_moneda_original').value))) + 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_adenda_moneda_original').value)));
    
        document.getElementById('proyectovigente_total_moneda_original').value  
                = fmtNum(document.getElementById('proyectovigente_total_moneda_original').value);
        
        
    };     
    proyectovigente_monto_moneda_original.onblur();       
        
    
    var proyectovigente_tipo_cambio_guaranies = document.getElementById('proyectovigente_tipo_cambio_guaranies');          
    proyectovigente_tipo_cambio_guaranies.onblur  = function() {                
        proyectovigente_tipo_cambio_guaranies.value  = fmtNum(proyectovigente_tipo_cambio_guaranies.value);
    };     
    proyectovigente_tipo_cambio_guaranies.onblur();       
        
    
    var proyectovigente_monto_guaranies = document.getElementById('proyectovigente_monto_guaranies');          
    proyectovigente_monto_guaranies.onblur  = function() {                
        proyectovigente_monto_guaranies.value  = fmtNum(proyectovigente_monto_guaranies.value);
        
        
        // suma      
         document.getElementById('proyectovigente_total_monto_guaranies').value = 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_guaranies').value))) + 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_guaranies_adenda').value)));
    
        document.getElementById('proyectovigente_total_monto_guaranies').value  
                = fmtNum(document.getElementById('proyectovigente_total_monto_guaranies').value);
                
    };     
    proyectovigente_monto_guaranies.onblur();       
            
    
    var proyectovigente_monto_adenda_moneda_original = document.getElementById('proyectovigente_monto_adenda_moneda_original');          
    proyectovigente_monto_adenda_moneda_original.onblur  = function() {                
        proyectovigente_monto_adenda_moneda_original.value  = fmtNum(proyectovigente_monto_adenda_moneda_original.value);
        
        
        // suma      
         document.getElementById('proyectovigente_total_moneda_original').value = 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_moneda_original').value))) + 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_adenda_moneda_original').value)));
    
        document.getElementById('proyectovigente_total_moneda_original').value  
                = fmtNum(document.getElementById('proyectovigente_total_moneda_original').value);
        
        
        
        
        // monto guaranies adenda
        document.getElementById('proyectovigente_monto_guaranies_adenda').value = 
                parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_adenda_moneda_original').value))) * 
                parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_tipo_cambio_adenda').value)));
        
        document.getElementById('proyectovigente_monto_guaranies_adenda').value  
                = fmtNum(document.getElementById('proyectovigente_monto_guaranies_adenda').value);        
        
        
        
    };     
    proyectovigente_monto_adenda_moneda_original.onblur();       
            
    
    
    var proyectovigente_monto_guaranies_adenda = document.getElementById('proyectovigente_monto_guaranies_adenda');          
    proyectovigente_monto_guaranies_adenda.onblur  = function() {                
        proyectovigente_monto_guaranies_adenda.value  = fmtNum(proyectovigente_monto_guaranies_adenda.value);
        
        // suma      
         document.getElementById('proyectovigente_total_monto_guaranies').value = 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_guaranies').value))) + 
            parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_guaranies_adenda').value)));
    
        document.getElementById('proyectovigente_total_monto_guaranies').value  
                = fmtNum(document.getElementById('proyectovigente_total_monto_guaranies').value);
                
    };     
    proyectovigente_monto_guaranies_adenda.onblur();       
    
    
    var proyectovigente_tipo_cambio_adenda = document.getElementById('proyectovigente_tipo_cambio_adenda');          
    proyectovigente_tipo_cambio_adenda.onblur  = function() {                
        proyectovigente_tipo_cambio_adenda.value  = fmtNum(proyectovigente_tipo_cambio_adenda.value);
        
        
        // monto guaranies adenda
        document.getElementById('proyectovigente_monto_guaranies_adenda').value = 
                parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_monto_adenda_moneda_original').value))) * 
                parseFloat(NumQP(fmtNum(document.getElementById('proyectovigente_tipo_cambio_adenda').value)));
        
        document.getElementById('proyectovigente_monto_guaranies_adenda').value  
                = fmtNum(document.getElementById('proyectovigente_monto_guaranies_adenda').value);      
    };     
    proyectovigente_tipo_cambio_adenda.onblur();       
    

    
    var proyectovigente_total_moneda_original = document.getElementById('proyectovigente_total_moneda_original');          
    proyectovigente_total_moneda_original.onblur  = function() {                
        proyectovigente_total_moneda_original.value  = fmtNum(proyectovigente_total_moneda_original.value);         
    };     
    proyectovigente_total_moneda_original.onblur();       
    
    
    
    var proyectovigente_total_monto_guaranies = document.getElementById('proyectovigente_total_monto_guaranies');          
    proyectovigente_total_monto_guaranies.onblur  = function() {                
        proyectovigente_total_monto_guaranies.value  = fmtNum(proyectovigente_total_monto_guaranies.value);
    };     
    proyectovigente_total_monto_guaranies.onblur();       
    
    
    
    
    var proyectovigente_importe_ejecucion = document.getElementById('proyectovigente_importe_ejecucion');          
    proyectovigente_importe_ejecucion.onblur  = function() {                
        proyectovigente_importe_ejecucion.value  = fmtNum(proyectovigente_importe_ejecucion.value);
    };     
    proyectovigente_importe_ejecucion.onblur();       

    
    var proyectovigente_saldo = document.getElementById('proyectovigente_saldo');          
    proyectovigente_saldo.onblur  = function() {                
        proyectovigente_saldo.value  = fmtNum(proyectovigente_saldo.value);
    };     
    proyectovigente_saldo.onblur();       
    
    


    
    
    
        
    //btn_enviar_1        
    var btn_enviar_1 = document.getElementById('btn_enviar_1');
    btn_enviar_1.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_1").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];

/*
        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);
        formdata.append("filetxt", filetxt);
  */ 
 
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);
 

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 1 )
            .then(( xhr ) => {                
                //document.getElementById( "divroot" ).innerHTML =  ""; 
        
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    
    
    
        
    //btn_enviar_2        
    var btn_enviar_2 = document.getElementById('btn_enviar_2');
    btn_enviar_2.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_2").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];

        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);
         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 2 )
            .then(( xhr ) => {                
                //document.getElementById( "divroot" ).innerHTML =  ""; 
        
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    
    
    

    
        
    //btn_enviar_3        
    var btn_enviar_3 = document.getElementById('btn_enviar_3');
    btn_enviar_3.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_3").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];

        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);
         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 3 )
            .then(( xhr ) => {                
                //document.getElementById( "divroot" ).innerHTML =  ""; 
        
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    
        

    
    //btn_enviar_4        
    var btn_enviar_4 = document.getElementById('btn_enviar_4');
    btn_enviar_4.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_4").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 4 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    
        
        
    
    //btn_enviar_5        
    var btn_enviar_5 = document.getElementById('btn_enviar_5');
    btn_enviar_5.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_5").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 5 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    


    //btn_enviar_6        
    var btn_enviar_6 = document.getElementById('btn_enviar_6');
    btn_enviar_6.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_6").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 6 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    
    
    

    //btn_enviar_7        
    var btn_enviar_7 = document.getElementById('btn_enviar_7');
    btn_enviar_7.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_7").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 7 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
        
    

    //btn_enviar_8        
    var btn_enviar_8 = document.getElementById('btn_enviar_8');
    btn_enviar_8.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_8").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 8 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    
    
    
    
    //btn_enviar_9        
    var btn_enviar_9 = document.getElementById('btn_enviar_9');
    btn_enviar_9.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_9").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 9 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
    
    
    
    
    //btn_enviar_10        
    var btn_enviar_10 = document.getElementById('btn_enviar_10');
    btn_enviar_10.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_10").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 10 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
        
        
        
    
    
    //btn_enviar_11        
    var btn_enviar_11 = document.getElementById('btn_enviar_11');
    btn_enviar_11.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_11").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 11 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
        
        
    
    
    //btn_enviar_12     
    var btn_enviar_12 = document.getElementById('btn_enviar_12');
    btn_enviar_12.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_12").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 12 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
        
        
     

    //btn_enviar_13       
    var btn_enviar_13 = document.getElementById('btn_enviar_13');
    btn_enviar_13.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_13").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 13 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
        
          
    
    

    //btn_enviar_14       
    var btn_enviar_14 = document.getElementById('btn_enviar_14');
    btn_enviar_14.onclick = function(event) {     
       
        var proyectoid  = document.getElementById('proyectovigente_id').value;  
        var filetxt = document.getElementById("pryecto_nombre_14").files[0];
        var nombre_archivo = filetxt.name;

        var exten = nombre_archivo.split(".");
        exten = exten[exten.length - 1];
        
        var  formdata = new FormData();
        formdata.append("filetxt", filetxt);         

        migracion_proceso_archivo_promesa( formdata, nombre_archivo, proyectoid, 14 )
            .then(( xhr ) => {                
                var obj = new ProyectoVigente();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, proyectoid );
            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });          
    }
        
              
            
            
    /*
    document.getElementById("proyectovigente_total_moneda_original").disabled = true;
    document.getElementById("proyectovigente_total_monto_guaranies").disabled = true;    
    */
    
    
};






ProyectoVigente.prototype.form_validar = function() {    
   
   
    
    var proyectovigente_proyecto_numero = document.getElementById('proyectovigente_proyecto_numero');    
    if (proyectovigente_proyecto_numero.value == "")         
    {
        msg.error.mostrar("Numero de proyecto vacio");           
        proyectovigente_proyecto_numero.focus();
        proyectovigente_proyecto_numero.select();        
        return false;
    }  
   
   
   
    var proyectovigente_proyecto_nombre = document.getElementById('proyectovigente_proyecto_nombre');    
    if (proyectovigente_proyecto_nombre.value == "")         
    {
        msg.error.mostrar("Nombre de proyecto vacio");           
        proyectovigente_proyecto_nombre.focus();
        proyectovigente_proyecto_nombre.select();        
        return false;
    }  
   
   
   
   
    var proyectovigente_convenio_contrato_numero = document.getElementById('proyectovigente_convenio_contrato_numero');    
    if (proyectovigente_convenio_contrato_numero.value == "")         
    {
        msg.error.mostrar("Convenio contrato numero vacio");           
        proyectovigente_convenio_contrato_numero.focus();
        proyectovigente_convenio_contrato_numero.select();        
        return false;
    }  
   
   
   
    var proyectovigente_meses_duracion = document.getElementById('proyectovigente_meses_duracion');    
    if (fmtNum(proyectovigente_meses_duracion.value) == 0)         
    {
        msg.error.mostrar("Duracion meses vacio");           
        proyectovigente_meses_duracion.focus();
        proyectovigente_meses_duracion.select();        
        return false;
    }  
   
   
   
   
   
   
    var proyectovigente_cordinador = document.getElementById('proyectovigente_cordinador');    
    if (fmtNum(proyectovigente_cordinador.value) == 0)         
    {
        msg.error.mostrar("Falta seleccionar cordinador de proyecto");           
        return false;
    }  
   
   
    return true;
};










ProyectoVigente.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }



    let promesa = arasa.vista.lista_paginacion(obj, page);
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
                        
                    
            // suma                
            var ojson = JSON.parse( xhr.responseText ) ; 
            var sum = ojson['summary'][0]['monto_guaranies']  ;  
            var cant = ojson['summary'][0]['cantidad']  ; 

            if (!(sum === undefined)) {    
                document.getElementById( "monto_guaranies" ).innerHTML =  fmtNum(sum.toString());                
                document.getElementById( "cantidad" ).innerHTML =  fmtNum(cant.toString());                
            }   
            else{
                document.getElementById( "monto_guaranies" ).innerHTML =  fmtNum("0");                
                document.getElementById( "cantidad" ).innerHTML =  fmtNum("0");                
            }



            // cuadro de busqueda            
            fetch(  html.url.absolute() + '/aplicacion/proyectovigente/'+ '/htmf/busqueda_bar.html' )
              .then(response => {
                return response.text();
              })
              .then(data => {
                document.getElementById( "bar_busqueda" ).innerHTML =  data;    
                busqueda_bar_accion(obj);
              })            

    
            acciones_main_list(obj);
    
            
        })
        /*
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 
    */

};









ProyectoVigente.prototype.carga_combos = function( obj  ) {                
    

    var proyectovigente_ente_financiador = document.getElementById("proyectovigente_ente_financiador");
    var idedovalue = proyectovigente_ente_financiador.value;


    var entef = new EnteFinanciador(); 
    entef.combobox("proyectovigente_ente_financiador");    



    var moneda = new Moneda(); 
    moneda.combobox("proyectovigente_moneda");    


    var tipoproyecto = new TipoProyecto(); 
    tipoproyecto.combobox("proyectovigente_tipo_proyecto");            


    var estadoproyecto = new EstadoProyecto(); 
    estadoproyecto.combobox("proyectovigente_estado_proyecto");            



};








ProyectoVigente.prototype.post_form_id = function( obj  ) {                



    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documentos']) ;       
    
   
    // debloquear los botones de seleccion de archivos
    
    
    document.getElementById( 'pryecto_nombre_1' ).disabled = false;
    document.getElementById( 'pryecto_nombre_2' ).disabled = false;
    document.getElementById( 'pryecto_nombre_3' ).disabled = false;
    document.getElementById( 'pryecto_nombre_4' ).disabled = false;
    document.getElementById( 'pryecto_nombre_5' ).disabled = false;
    document.getElementById( 'pryecto_nombre_6' ).disabled = false;
    document.getElementById( 'pryecto_nombre_7' ).disabled = false;
    
    document.getElementById( 'pryecto_nombre_8' ).disabled = false;
    document.getElementById( 'pryecto_nombre_9' ).disabled = false;
    document.getElementById( 'pryecto_nombre_10' ).disabled = false;
    document.getElementById( 'pryecto_nombre_11' ).disabled = false;
    document.getElementById( 'pryecto_nombre_12' ).disabled = false;
    document.getElementById( 'pryecto_nombre_13' ).disabled = false;    
    document.getElementById( 'pryecto_nombre_14' ).disabled = false;        

    
    document.getElementById( 'fileD_1').style.display = "none";  
    document.getElementById( 'fileD_2').style.display = "none";  
    document.getElementById( 'fileD_3').style.display = "none";  
    document.getElementById( 'fileD_4').style.display = "none";      
    document.getElementById( 'fileD_5').style.display = "none";  
    document.getElementById( 'fileD_6').style.display = "none";  
    document.getElementById( 'fileD_7').style.display = "none";      
    
    document.getElementById( 'fileD_8').style.display = "none";  
    document.getElementById( 'fileD_9').style.display = "none";      
    document.getElementById( 'fileD_10').style.display = "none";          
    document.getElementById( 'fileD_11').style.display = "none";                  
    document.getElementById( 'fileD_12').style.display = "none";                  
    document.getElementById( 'fileD_13').style.display = "none";                      
    document.getElementById( 'fileD_14').style.display = "none";                          
    
    
    //if (typeof json === 'undefined') 
    if (json === '[]') 
    { 
        //document.getElementById( 'fileD' ).style.display = "none";   
                //result.innerHTML = "Variable is Undefined"; 
    } 
    else 
    {   
        // mostrar los documentos
        var ofiles = JSON.parse( json ) ;   
        
        // campo_01 = ofiles['file_name']
        
        const jsonData = JSON.parse(json);
        

        for (let i = 0; i < jsonData.length; i++) {
            const item = jsonData[i];
            
            //if (item.campo_numero == 1 ) { 
                                                    
                document.getElementById( 'fileU_'+item.campo_numero ).style.display = "none";  
                document.getElementById( 'fileD_'+item.campo_numero).style.display = "flex";          
                                
                
                document.getElementById( 'fileD_nombre_' + item.campo_numero  ).innerHTML  
                    = item.file_name
       
       
                document.getElementById( 'btn_cambiar_'+ item.campo_numero).onclick = function()
                {                  
                    document.getElementById( 'fileD_'+item.campo_numero ).style.display = "none";  
                    document.getElementById( 'fileU_'+item.campo_numero).style.display = "flex";     
                };   
                
                //var afileD = document.getElementById( 'afileD');
                //afileD.onclick = function()
                document.getElementById( 'fileD_nombre_' + item.campo_numero).onclick = function()
                {  
                    var url = html.url.absolute() + "/ProyectoVigente/Download";

                    // Use XMLHttpRequest instead of Jquery $ajax
                    xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        var a;
                        if (xhttp.readyState === 4 && xhttp.status === 200) {
                            // Trick for making downloadable link
                            a = document.createElement('a');
                            a.href = window.URL.createObjectURL(xhttp.response);
                            // Give filename you wish to download

                            var file_name =  xhttp.getResponseHeader("file_name") ; 

                            a.download = file_name;
                            a.style.display = 'none';
                            document.body.appendChild(a);
                            a.click();
                        }
                    };
                    
                    // Post data to URL which handles post request
                    xhttp.open("GET", url);
                    xhttp.setRequestHeader("Content-Type", "application/json");
                    xhttp.setRequestHeader("regid", document.getElementById( 'proyectovigente_id' ).value );
                    xhttp.setRequestHeader("campo_numero", item.campo_numero );

                    // You should set responseType as blob for binary responses
                    xhttp.responseType = 'blob';
                    xhttp.send();

                }                
            //}
            // fin item
  
        }        
    } 
    
    
    
};




ProyectoVigente.prototype.getUrlFiltro = function( obj  ) {                
    
    var ret = "";    
    ret = obj.filtro;

    //ret = ";ente=5;moneda=0;cordinador=0;tipo=0;z=a";
    
/*    
    var busquedatexto = document.getElementById("busquedatexto");
    console.log(busquedatexto);

var simple = document.getElementById( "busqueda_simple" )
if ( document.getElementById( "busqueda_simple" )) {
    
      alert( simple.style.display );
}
*/
  

/*
    if (!(busquedatexto === null)) {                
        ret = ";q="+busquedatexto.value;
    }
  */  

    return ret;
    
};









ProyectoVigente.prototype.preedit = function( obj  ) {                


    document.getElementById("proyectovigente_monto_guaranies_adenda").disabled = true;
    document.getElementById("proyectovigente_total_moneda_original").disabled = true;
    document.getElementById("proyectovigente_total_monto_guaranies").disabled = true;    
    
};



