

function Departamento(){
    
   this.tipo = "departamento";   
   this.recurso = "departamentos";   
   this.value = 0;
   this.form_descrip = "moneda_nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/vacc";   
   
   
   this.titulosin = "Departamento"
   this.tituloplu = "Departamento"   
      
   
   this.campoid=  'departamento';
   this.tablacampos =  ['departamento', 'nombre'];
   
   this.etiquetas =  ['Deoartamento', 'Nombre'];                                  
    
   this.tablaformat = ['C', 'C'];                                  
   
   this.tbody_id = "departamento-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "departamento-acciones";   
         
   this.parent = null;
   
   
}





Departamento.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);  
    
    var departamento_departamento = document.getElementById('departamento_departamento'); 
    departamento_departamento.disabled = false;
    
    
};





Departamento.prototype.form_ini = function() {    
    
};






Departamento.prototype.form_validar = function() {    
    
    return true;
};










Departamento.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            


        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Departamento.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['departamento'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}


