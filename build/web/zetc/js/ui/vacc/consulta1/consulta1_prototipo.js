 
 
 function Consulta1(){
    
   this.tipo = "vacunacion";   
   this.recurso = "vacunaciones";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/vacc";   
   
   
   this.titulosin = "Registro de Vacunacion"
   this.tituloplu = "Vacunaciones"   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['id', 'fecha_reporte', 'registrador.nombre', 'vacunador.nombre',
        'departamento.nombre', 'distrito.nombre',
        'cantidad_50_mas', 'cantidad_18_49', 'cantidad_18_menos', 'cantidad_vacunados_total',
        'transporte'];
   
   this.etiquetas =  ['Codigo', 'Fecha', 'Registrador', 'Vacunador',
        'Departamento', 'Distrito',
        'cant 50+', 'Cant 18-49', 'Cant 18-', 'Cant total',
        'transporte'];                                  
    
   this.tablaformat = ['N', 'D', 'C', 'C', 'C', 'C',
                'N', 'N','N','N', 'C' ];                                  
   
   this.tbody_id = "vacunacion-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "vacunacion-acciones";   
   
}









Consulta1.prototype.form_ini = function() {    
  
  
      
    var dept = new Departamento(); 
    dept.combobox("departamento");      
    var domobj = document.getElementById("departamento");
    var opt = document.createElement('option');            
    opt.value = 0;
    opt.innerHTML = "Todos";                        
    domobj.appendChild(opt); 
  
  
  
    // filtrar ciudades  segun valor departamento
    var departamento = document.getElementById('departamento');   
    departamento.onchange  = function() {   
    
        var dptoid = departamento.value;        
                
        var distrito = new Distrito(); 
        distrito.combobox_dpto_all("distrito", dptoid);   
        
    }    
    
    var domobj = document.getElementById("distrito");
    var opt = document.createElement('option');            
    opt.value = 0;
    opt.innerHTML = "Todos";                        
    domobj.appendChild(opt);     
    
  
  

    
    var registrador = document.getElementById('registrador');   
    registrador.onblur  = function() {      
                  
        registrador.value = fmtNum(registrador.value);      
        registrador.value = NumQP(registrador.value);      
    
        var  id = (registrador.value );
        
        
        ajax.url = html.url.absolute() +'/api/registradores/'+id;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {                               
                    var ojson = JSON.parse(xhr.responseText) ;     
                                      
                    document.getElementById('cedula_registrador').value = ojson['cedula'];
                   
                    document.getElementById('registrador_descripcion_out').innerHTML = 
                            ojson['nombre'];
                    
                    
                }
                else{
                    
                    document.getElementById('cedula_registrador').value = 0;                   
                    document.getElementById('registrador_descripcion_out').innerHTML = "Todos";
                    
                    
                    
                }                                   

            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
             
     };      
    registrador.onblur();       
        
    
    
    var ico_more_registrador = document.getElementById('ico-more-registrador');
    ico_more_registrador.addEventListener('click',
        function(event) {     

            var obj = new Registrador();      
            
            obj.acctionresul = function(id) {    
                registrador.value = id; 
                registrador.onblur(); 
            };       
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);

        },
        false
    );        
         
    
    
    
    var cedula_registrador = document.getElementById('cedula_registrador');   
    cedula_registrador.onblur  = function() {                        
                  
        cedula_registrador.value = fmtNum(cedula_registrador.value);      
        cedula_registrador.value = NumQP(cedula_registrador.value);      
    
        var  cedu = (cedula_registrador.value );

        ajax.url =  html.url.absolute()+'/api/registradores/cedula/'+cedu;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {     
                    var ojson = JSON.parse(xhr.responseText) ;     
                    
                    registrador.value = ojson['registrador']; 
                    registrador.onblur();     
                    
                }
                else{
                    
                    registrador.value = 0; 
                    registrador.onblur();                                       
                }                  
                
            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            }); 
    
     };    
    
    
    
    
    
    // vacunador
    
    var vacunador = document.getElementById('vacunador');   
    vacunador.onblur  = function() {      
                  
        vacunador.value = fmtNum(vacunador.value);      
        vacunador.value = NumQP(vacunador.value);      
    
        var  id = (vacunador.value );        
        
        ajax.url = html.url.absolute() +'/api/vacunadores/'+id;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {                               
                    var ojson = JSON.parse(xhr.responseText) ;     
                   
                    document.getElementById('vacunador_descripcion_out').innerHTML = 
                            ojson['nombre'];
                    
                    document.getElementById('cedula_vacunador').value = ojson['cedula'];
                    
                
                }
                else{
                   document.getElementById('vacunador_descripcion_out').innerHTML = "Todos";
                   document.getElementById('cedula_vacunador').value = 0;
                                      
                }                                   

            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
             
     };      
    vacunador.onblur();       
    
    

    var ico_more_vacunador = document.getElementById('ico-more-vacunador');
    ico_more_vacunador.addEventListener('click',
        function(event) {     

            var obj = new Vacunador();      
            
            obj.acctionresul = function(id) {    
                vacunador.value = id; 
                vacunador.onblur(); 
            };       
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);

        },
        false
    );        
        
    
    var cedula_vacunador = document.getElementById('cedula_vacunador');   
    cedula_vacunador.onblur  = function() {                        
                  
        cedula_vacunador.value = fmtNum(cedula_vacunador.value);      
        cedula_vacunador.value = NumQP(cedula_vacunador.value);      
    
        var  cedu = (cedula_vacunador.value );

        ajax.url =  html.url.absolute() +'/api/vacunadores/cedula/'+cedu;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {            
                    
                    var ojson = JSON.parse(xhr.responseText) ;     
                    
                    vacunador.value = ojson['vacunador']; 
                    vacunador.onblur();     
                    
                }
                else{
                    
                    vacunador.value = 0; 
                    vacunador.onblur();                                       
                }                   
                
            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            }); 
    
     };      
    //cedula_vacunador.onblur();       
    
};






Consulta1.prototype.form_validar = function() {    
    
    return true;
};








Consulta1.prototype.main_list = function(obj) {    


    fetch(  html.url.absolute() + '/vacc/consulta1/htmf/form.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        document.getElementById( "arti_cab" ).innerHTML =  data;    


       this.form_ini();
       

            fetch(  html.url.absolute() + '/vacc/consulta1/htmf/lista.html' )
              .then(response => {
                return response.text();
              })
              .then(data => {
                document.getElementById( "arti_det" ).innerHTML =  data;    
        
        
                document.getElementById( "btn_consultar" ).onclick = function()
                {                  
                    var co = new Consulta1;
                    co.detalle_promesa(obj, 1);
                    
                };          
        
                
                

                var btn_xlsx = document.getElementById('btn_xlsx');
                btn_xlsx.addEventListener('click',
                    function(event) {     


                        var url = html.url.absolute()+"/consulta1.xlsx"
                            +"?dpt="+document.getElementById("departamento").value 
                            +"&dis="+document.getElementById("distrito").value
                            +"&fe1="+document.getElementById("qry_fecha_inicio").value
                            +'&fe2='+document.getElementById("qry_fecha_finalizacion").value 
                            +'&reg='+document.getElementById("registrador").value 
                            +'&vac='+document.getElementById("vacunador").value 
                            ;
                        
                        
                        
                        
                        var xhr =  new XMLHttpRequest();                           
                       

                        xhr.onreadystatechange = function() {
                            if(xhr.readyState === xhr.HEADERS_RECEIVED) {
                                //console.log('Request has started');
                            }
                        } ;                   


                        //ajax.xhr.onload = function (e) {
                        xhr.onload = function (e) {

                            if (xhr.readyState === 4 && xhr.status === 200) {

                                var contenidoEnBlob = xhr.response;
                                var link = document.createElement('a');
                                link.href = (window.URL || window.webkitURL).createObjectURL(contenidoEnBlob);

                                link.download = "registradas.xlsx";

                                var clicEvent = new MouseEvent('click', {
                                    'view': window,
                                    'bubbles': true,
                                    'cancelable': true
                                });
                                //Simulamos un clic del usuario
                                //no es necesario agregar el link al DOM.
                                link.dispatchEvent(clicEvent);
                                //link.click();                            
                                //ajax.headers.get();                    

                            }
                            else 
                            {
                                if (xhr.status === 401){
                                    var state = xhr.status;     
                                    arasa.html.url.redirect(state);  
                                }
                                else{
                                    alert(" No es posible acceder al archivo");
                                }

                            }

                        };                    



                        xhr.open("POST", url, true);
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                        var type = "application/json";
                        xhr.setRequestHeader('Content-Type', type);   
                        xhr.setRequestHeader("token", localStorage.getItem('token'));  
                        
                        xhr.responseType = 'blob';
                        xhr.send( null );                       


                    },
                    false
                );    

                
                
              })            


      })            






};





Consulta1.prototype.detalle_promesa = function( obj, page   ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();
            obj.page = page;


///consulta1/;dpt=0;dis=0;fe1=20000101;fe2=22220101;reg=0;vac=0?page=1

            //var url = html.url.absolute() + '/api/vacunaciones/consulta/:aa=999' ;    
            var url = html.url.absolute() + '/api/vacunaciones/consulta1/'
                +';dpt='+document.getElementById("departamento").value 
                +';dis='+document.getElementById("distrito").value 
                +';fe1='+document.getElementById("qry_fecha_inicio").value 
                +';fe2='+document.getElementById("qry_fecha_finalizacion").value 
                +';reg='+document.getElementById("registrador").value 
                +';vac='+document.getElementById("vacunador").value 
                +'?page='+page;

            var xhr =  new XMLHttpRequest();      


//            reflex.ini(obj);            
//            url = reflex.getApi(obj, page, busca);   

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;


                    var ojson = JSON.parse( tabla.json ) ;     
                    
                    
                    if (tabla.json != "{}"){
                        tabla.json = JSON.stringify(ojson['datos']) ;                                
                        
                        // suma de consultas
                        var jsumas = JSON.stringify(ojson['summary']) ;              
                        
                        var ojsumas = JSON.parse(jsumas) ;     
                        
                        document.getElementById("sum_50").innerHTML
                            =  fmtNum( ojsumas["sum_50"] );
                        
                        document.getElementById("sum_49").innerHTML
                            =  fmtNum( ojsumas["sum_49"] );
                        
                        document.getElementById("sum_18").innerHTML
                            =  fmtNum( ojsumas["sum_18"] );
                        
                        document.getElementById("sum_total").innerHTML
                            =  fmtNum( ojsumas["sum_total"] );
                        
                        
                        
                        
                    }
                    

                    tabla.ini(obj);
                    tabla.gene();              
                    tabla.formato(obj);
  
  
  
                    //paginacion  
  
                    if (tabla.json != "{}"){
                        var json_paginacion = JSON.stringify(ojson['paginacion']) ;     
    
                        arasa.html.paginacion.ini(json_paginacion);

                        document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                                = arasa.html.paginacion.gene();                           
                        
                        var co = new Consulta1;
                        co.paginacion_move(obj);

                        
                    }
                    else{
                        document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                            = "";                           
                    }

                        resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};



Consulta1.prototype.paginacion_move = function( obj ) {    

            
        var listaUL = document.getElementById( obj.tipo + "_paginacion" );
        var uelLI = listaUL.getElementsByTagName('li');


        var pagina = 0;


        for (var i=0 ; i < uelLI.length; i++)
        {
            var lipag = uelLI[i];   

            if (lipag.dataset.pagina == "act"){                                     
                pagina = lipag.firstChild.innerHTML;
            }                    
        }



        for (var i=0 ; i < uelLI.length; i++)
        {
            var datapag = uelLI[i].dataset.pagina;     

            if (!(datapag == "act"  || datapag == "det"  ))
            {
                uelLI[i].addEventListener ( 'click',
                    function() {                                      

                        switch (this.dataset.pagina)
                        {
                           case "sig": 
                                   pagina = parseInt(pagina) +1;
                                   break;                                                                          

                           case "ant":                                     
                                   pagina = parseInt(pagina) -1;
                                   break;

                           default:  
                                   pagina = this.childNodes[0].innerHTML.toString().trim();
                                   break;
                        }
                        pagina = parseInt( pagina , 10);                                                                       

                        
                        //var co = new Consulta1;
                        obj.detalle_promesa(obj, pagina);

                        //tabla.refresh_promise( obj, pagina, busca, fn  ) ;

                    },
                    false
                );                
            }            
        }           

};
