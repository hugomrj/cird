--
-- PostgreSQL database dump
--


ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN fecha_adenda date;

ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN observacion character varying(250);


ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN monto_guaranies_adenda bigint;

ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN tipo_cambio_adenda bigint;

ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN importe_ejecucion bigint;

ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN saldo bigint;

ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN monto_adenda_moneda_original bigint;


ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN total_moneda_original bigint;


ALTER TABLE aplicacion.proyectos_vigentes
  ADD COLUMN total_monto_guaranies bigint;
  
ALTER TABLE aplicacion.proyectos_vigentes RENAME monto_total_moneda_original  TO monto_moneda_original;



UPDATE aplicacion.proyectos_vigentes
SET
observacion='',
monto_guaranies_adenda=0,
tipo_cambio_adenda=0,
importe_ejecucion=0,
saldo=0,
monto_adenda_moneda_original=0,
total_moneda_original=0,
total_monto_guaranies=0
;




ALTER TABLE aplicacion.proyectos_vigentes_documentos
  ADD COLUMN campo_numero integer;



ALTER TABLE aplicacion.proyectos_vigentes_documentos
  DROP CONSTRAINT proyectos_vigentes_documentos_proyectoid_key;


ALTER TABLE aplicacion.proyectos_vigentes_documentos
  ADD UNIQUE (proyectoid, campo_numero);



UPDATE aplicacion.proyectos_vigentes_documentos
SET campo_numero=1;