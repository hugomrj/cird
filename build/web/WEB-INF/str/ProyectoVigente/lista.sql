
select e.nombre xente_financiador,   
m.nombre xmoneda, c.nombre xcordinador, tp.nombre xtipoproyecto,  
ep.nombre xestadoproyecto,  
p.*   
from aplicacion.proyectos_vigentes p inner join aplicacion.entes_financiadores e  
on (p.ente_financiador = e.ente_financiador)  
inner join aplicacion.cordinadores c on (p.cordinador = c.cordinador)  
inner join aplicacion.monedas m on (p.moneda = m.moneda) 
inner join aplicacion.tipo_proyectos tp on (p.tipo_proyecto = tp.tipo_proyecto) 
inner join aplicacion.estados_proyectos ep  on (p.estado_proyecto = ep.estado_proyecto) 
where ( proyecto_nombre ilike '%v0%'    
	or e.nombre ilike '%v0%' 
	or convenio_contrato_numero ilike '%v0%' 
	or c.nombre ilike '%v0%' 
)  
order by  id  



