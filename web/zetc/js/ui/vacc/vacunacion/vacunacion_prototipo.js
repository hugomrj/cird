

function Vacunacion(){
    
   this.tipo = "vacunacion";   
   this.recurso = "vacunaciones";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/vacc";   
   
   
   this.titulosin = "Registro de Vacunacion"
   this.tituloplu = "Vacunaciones"   
      
   
   this.campoid=  'id';
   this.tablacampos =  ['id', 'fecha_reporte', 'registrador.nombre', 'vacunador.nombre',
        'departamento.nombre', 'distrito.nombre',
        'cantidad_50_mas', 'cantidad_18_49', 'cantidad_18_menos', 'cantidad_vacunados_total',
        'transporte'];
   
   this.etiquetas =  ['Codigo', 'Fecha', 'Registrador', 'Vacunador',
        'Departamento', 'Distrito',
        'cant 50+', 'Cant 18-49', 'Cant 18-', 'Cant total',
        'transporte'];                                  
    
   this.tablaformat = ['N', 'D', 'C', 'C', 'C', 'C',
                'N', 'N','N','N', 'C' ];                                  
   
   this.tbody_id = "vacunacion-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "vacunacion-acciones";   
         
   this.parent = null;
   
    this.combobox = 
          {
            "departamento":{
                 "value":"departamento",
                 "inner":"nombre"
              },
            "distrito":{
                 "value":"distrito",
                 "inner":"nombre"
              },
          };      

    
   
}





Vacunacion.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);    
    
    obj.form_ini();
    obj.form_accion();
    
    
    document.getElementById('id_sel_transporte').style.display="block";   
    document.getElementById('id_vacunacion_transporte').style.display="none";       
    
    

    // no mostrar form de archivo
    var form_archivo = document.getElementById('form_archivo');      
    form_archivo.style.display = "none";
    
 
    
    
};





Vacunacion.prototype.form_ini = function() {   
    
    
    var cedula_registrador = document.getElementById('cedula_registrador');          
    cedula_registrador.onblur  = function() {                
        cedula_registrador.value  = fmtNum(cedula_registrador.value);
    };     
    cedula_registrador.onblur();       
        
    
        
    var vacunacion_cantidad_50_mas = document.getElementById('vacunacion_cantidad_50_mas');          
    vacunacion_cantidad_50_mas.onblur  = function() {                
        vacunacion_cantidad_50_mas.value  = fmtNum(vacunacion_cantidad_50_mas.value);
    };     
    vacunacion_cantidad_50_mas.onblur();       
        

    var vacunacion_cantidad_18_49 = document.getElementById('vacunacion_cantidad_18_49');          
    vacunacion_cantidad_18_49.onblur  = function() {                
        vacunacion_cantidad_18_49.value  = fmtNum(vacunacion_cantidad_18_49.value);
    };     
    vacunacion_cantidad_18_49.onblur();       

    var vacunacion_cantidad_18_menos = document.getElementById('vacunacion_cantidad_18_menos');          
    vacunacion_cantidad_18_menos.onblur  = function() {                
        vacunacion_cantidad_18_menos.value  = fmtNum(vacunacion_cantidad_18_menos.value);
    };     
    vacunacion_cantidad_18_menos.onblur();       
                    

    var vacunacion_cantidad_vacunados_total = document.getElementById('vacunacion_cantidad_vacunados_total');          
    vacunacion_cantidad_vacunados_total.onblur  = function() {                
        vacunacion_cantidad_vacunados_total.value  = fmtNum(vacunacion_cantidad_vacunados_total.value);
    };     
    vacunacion_cantidad_vacunados_total.onblur();     
    
    vacunacion_cantidad_vacunados_total.disabled = true;
    
    
    
    
    
    // boton enviar
    var btn_enviar = document.getElementById('btn_enviar');
    btn_enviar.onclick = function(event) {     
       

        var vacunacionid  = document.getElementById('vacunacion_id').value;        
        
        var filetxt = document.getElementById("migracion_file").files[0];

        var nombre_archivo = filetxt.name;


        var form = document.getElementById('form_archivo');
        var formdata = new FormData(form);

        formdata.append("filetxt", filetxt);



        migracion_proceso_promesa( formdata, nombre_archivo, vacunacionid )
            .then(( xhr ) => {
                
                //document.getElementById( "divroot" ).innerHTML =  ""; 
        
                var obj = new Vacunacion();     
                obj.dom = 'arti_form';
                reflex.form_id_promise( obj, vacunacionid );

            })
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
        
    }
          
        
    
        
    
    
    
};


 

Vacunacion.prototype.form_accion = function() {    
    
    
    
    var vacunacion_registrador = document.getElementById('vacunacion_registrador');   
    vacunacion_registrador.onblur  = function() {      
                  
        vacunacion_registrador.value = fmtNum(vacunacion_registrador.value);      
        vacunacion_registrador.value = NumQP(vacunacion_registrador.value);      
    
        var  id = (vacunacion_registrador.value );
        
        
        ajax.url = html.url.absolute() +'/api/registradores/'+id;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {                               
                    var ojson = JSON.parse(xhr.responseText) ;     
                                      
                    document.getElementById('cedula_registrador').value = ojson['cedula'];
                   
                    document.getElementById('registrador_descripcion_out').innerHTML = 
                            ojson['nombre'];
                    
                    document.getElementById('registrador_correo_descripcion_out').innerHTML = 
                            ojson['email'];
                    
                }
                else{
                    
                    document.getElementById('cedula_registrador').value = 0;                   
                    document.getElementById('registrador_descripcion_out').innerHTML = "";
                    document.getElementById('registrador_correo_descripcion_out').innerHTML = "";
                    
                }                                   

            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
             
     };      
    vacunacion_registrador.onblur();       
        
    
    
    var ico_more_registrador = document.getElementById('ico-more-registrador');
    ico_more_registrador.addEventListener('click',
        function(event) {     

            var obj = new Registrador();      
            
            obj.acctionresul = function(id) {    
                vacunacion_registrador.value = id; 
                vacunacion_registrador.onblur(); 
            };       
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);

        },
        false
    );        
            
    
    
    var cedula_registrador = document.getElementById('cedula_registrador');   
    cedula_registrador.onblur  = function() {                        
                  
        cedula_registrador.value = fmtNum(cedula_registrador.value);      
        cedula_registrador.value = NumQP(cedula_registrador.value);      
    
        var  cedu = (cedula_registrador.value );

        ajax.url =  html.url.absolute()+'/api/registradores/cedula/'+cedu;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {     
                    var ojson = JSON.parse(xhr.responseText) ;     
                    
                    vacunacion_registrador.value = ojson['registrador']; 
                    vacunacion_registrador.onblur();     
                    
                }
                else{
                    
                    vacunacion_registrador.value = 0; 
                    vacunacion_registrador.onblur();                                       
                }                  
                
            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            }); 
    
     };    
    
    
    
    
    
    
    
    
    
    var vacunacion_vacunador = document.getElementById('vacunacion_vacunador');   
    vacunacion_vacunador.onblur  = function() {      
                  
        vacunacion_vacunador.value = fmtNum(vacunacion_vacunador.value);      
        vacunacion_vacunador.value = NumQP(vacunacion_vacunador.value);      
    
        var  id = (vacunacion_vacunador.value );        
        
        ajax.url = html.url.absolute() +'/api/vacunadores/'+id;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {                               
                    var ojson = JSON.parse(xhr.responseText) ;     
                   
                    document.getElementById('vacunador_descripcion_out').innerHTML = 
                            ojson['nombre'];
                    
                    document.getElementById('cedula_vacunador').value = ojson['cedula'];
                    
                
                }
                else{
                   document.getElementById('vacunador_descripcion_out').innerHTML = "";
                   document.getElementById('cedula_vacunador').value = 0;
                                      
                }                                   

            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
             
     };      
    vacunacion_vacunador.onblur();       
    
    

    var ico_more_vacunador = document.getElementById('ico-more-vacunador');
    ico_more_vacunador.addEventListener('click',
        function(event) {     

            var obj = new Vacunador();      
            
            obj.acctionresul = function(id) {    
                vacunacion_vacunador.value = id; 
                vacunacion_vacunador.onblur(); 
            };       
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);

        },
        false
    );        
        
    
    var cedula_vacunador = document.getElementById('cedula_vacunador');   
    cedula_vacunador.onblur  = function() {                        
                  
        cedula_vacunador.value = fmtNum(cedula_vacunador.value);      
        cedula_vacunador.value = NumQP(cedula_vacunador.value);      
    
        var  cedu = (cedula_vacunador.value );

        ajax.url =  html.url.absolute() +'/api/vacunadores/cedula/'+cedu;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {            
                    
                    var ojson = JSON.parse(xhr.responseText) ;     
                    
                    vacunacion_vacunador.value = ojson['vacunador']; 
                    vacunacion_vacunador.onblur();     
                    
                }
                else{
                    
                    vacunacion_vacunador.value = 0; 
                    vacunacion_vacunador.onblur();                                       
                }                   
                
            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            }); 
    
     };      
    //cedula_vacunador.onblur();       
    
    
    
    //var s1 = document.getElementById( "vacunacion_cantidad_50_mas" );
    var v1 = document.getElementById('vacunacion_cantidad_50_mas');   
    v1.onblur  = function() {        
        sum_vacunas( )
    }
    var v2 = document.getElementById('vacunacion_cantidad_18_49');   
    v2.onblur  = function() {        
        sum_vacunas( )
    }
    var v3 = document.getElementById('vacunacion_cantidad_18_menos');   
    v3.onblur  = function() {        
        sum_vacunas( )
    }
    
    
    
    // sino    
    var sel_transporte = document.getElementById('sel_transporte');   
    sel_transporte.onchange  = function() {   
    
//        var dptoid = sel_transporte.value;      
        document.getElementById('vacunacion_transporte').value =    
                sel_transporte.value;
    }    
    sel_transporte.onchange();
        
    
    
    
    
    
};






Vacunacion.prototype.form_validar = function() {    
    
    
    var vacunacion_fecha_reporte = document.getElementById('vacunacion_fecha_reporte');
    if (vacunacion_fecha_reporte.value == ""){
        msg.error.mostrar("Fecha vacia");                    
        vacunacion_fecha_reporte.focus();
        vacunacion_fecha_reporte.select();                                       
        return false;        
    }
        
    
    var vacunacion_registrador = document.getElementById('vacunacion_registrador');        
    if (parseInt(NumQP(vacunacion_registrador.value)) <= 0 )         
    {
        msg.error.mostrar("Falta seleccionar registrador");    
        var cedula_registrador = document.getElementById('cedula_registrador');  
        cedula_registrador.focus();
        cedula_registrador.select();                
        return false;
    }     
    

    var vacunacion_vacunador = document.getElementById('vacunacion_vacunador');        
    if (parseInt(NumQP(vacunacion_vacunador.value)) <= 0 )         
    {
        msg.error.mostrar("Falta seleccionar vacunador");    
        var cedula_vacunador = document.getElementById('cedula_vacunador');  
        cedula_vacunador.focus();
        cedula_vacunador.select();                
        return false;
    }     
        
    
    
 /*  
    var nombre = document.getElementById('cordinador_nombre');    
    if (nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        nombre.focus();
        nombre.select();        
        return false;
    }              
   */ 
  
    return true;
};










Vacunacion.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            
            
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
                        
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );                          
   

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Vacunacion.prototype.carga_combos = function(  ) {                
    
    var dpto = new Departamento(); 
    dpto.combobox("vacunacion_departamento");    


    // filtrar ciudades  segun valor departamento
    var vacunacion_departamento = document.getElementById('vacunacion_departamento');   
    vacunacion_departamento.onchange  = function() {   
    
        var dptoid = vacunacion_departamento.value;        
        
        var distrito = new Distrito(); 
        distrito.combobox_dpto("vacunacion_distrito", dptoid);   
        
    }    
    
    var distrito = new Distrito(); 
    distrito.combobox_dpto("vacunacion_distrito", 1);  
    


};





Vacunacion.prototype.post_form_id = function( obj  ) {                

    document.getElementById('id_sel_transporte').style.display="none";   
    document.getElementById('id_vacunacion_transporte').style.display="block";   
    
    /*
    console.log("form.json");
    console.log(form.json);
    */
    
    var ojson = JSON.parse(form.json) ; 
    
    console.log(ojson["registrador"]["cedula"] )
    console.log(ojson["registrador"]["nombre"] )
    console.log(ojson["registrador"]["email"] )
    
    
    
    
    document.getElementById('cedula_registrador').value = ojson["registrador"]["cedula"] ;  
    document.getElementById('registrador_correo_descripcion_out').innerHTML
            = ojson["registrador"]["email"] ;  
    
    
    document.getElementById('cedula_vacunador').value = ojson["vacunador"]["cedula"] ;  
    
    
    
    // down
    
    var ojson = JSON.parse( form.json ) ;   
    var json = JSON.stringify(ojson['documento']) ;       
    
    if (typeof json === 'undefined') 
    { 
        document.getElementById( 'fileD' ).style.display = "none";   
                //result.innerHTML = "Variable is Undefined"; 
    } 
    else 
    {   
        document.getElementById( 'fileU' ).style.display = "none";   

        var ojson = JSON.parse( json ) ;                                            
        document.getElementById( 'fileD_nombre' ).innerHTML  
                = ojson['file_name']

        var afileD = document.getElementById( 'afileD');
        afileD.onclick = function()
        {  
            var url = html.url.absolute() + "/Vacunacion/Download";
            
            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                var a;
                if (xhttp.readyState === 4 && xhttp.status === 200) {
                    // Trick for making downloadable link
                    a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    // Give filename you wish to download

                    var file_name =  xhttp.getResponseHeader("file_name") ; 
                    
                    a.download = file_name;
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.setRequestHeader("regid", document.getElementById( 'vacunacion_id' ).value );

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.send();

        }
            

        var btn_cambiar = document.getElementById( 'btn_cambiar');
        btn_cambiar.onclick = function()
        {  
            document.getElementById( 'fileD' ).style.display = "none";   
            document.getElementById( 'fileU' ).style.display = "initial";  
        };   
    
    }
    
    
    
    
    
    
};




Vacunacion.prototype.preedit = function( obj  ) {                

    var id_sel_transporte = document.getElementById('id_sel_transporte');  
    id_sel_transporte.style.display="block";   
    
    var id_vacunacion_transporte = document.getElementById('id_vacunacion_transporte');  
    id_vacunacion_transporte.style.display="none";   
    
    
    var vacunacion_transporte = document.getElementById('vacunacion_transporte');  
    if (vacunacion_transporte.value == "Si"){
        document.getElementById('sel_transporte').selectedIndex = "0";    
    }
    if (vacunacion_transporte.value == "No"){
        document.getElementById('sel_transporte').selectedIndex = "1";    
    }
    
    obj.form_accion();
    
    //var index = selectElem.selectedIndex;
    //id_sel_transporte.selectedIndex = "2";
    
    //alert( document.getElementById('sel_transporte').selectedIndex );    
    

    

    
    
    
};





