

function Distrito(){
    
   this.tipo = "distrito";   
   this.recurso = "distritos";   
   this.value = 0;
   this.form_descrip = "distrito_nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/vacc";   
   
   
   this.titulosin = "Distrito"
   this.tituloplu = "Distritos"   
      
   
   this.campoid=  'distrito';
   this.tablacampos =  ['distrito', 'nombre', 'departamento.nombre' ];
   
   this.etiquetas =  ['Distrito', 'Nombre', 'Departamento'];                                  
    
   this.tablaformat = ['C', 'C', 'C'];                                  
   
   this.tbody_id = "distrito-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "distrito-acciones";   
         
   this.parent = null;
   
      
   this.combobox = 
            {
                "departamento":{
                   "value":"departamento",
                   "inner":"nombre"
                }
            };      
   

   
   
}





Distrito.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);  
    
};





Distrito.prototype.form_ini = function() {    
    
};






Distrito.prototype.form_validar = function() {    
    
    return true;
};










Distrito.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            


        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};






Distrito.prototype.carga_combos = function( obj  ) {                
    
/*
    var proyectovigente_ente_financiador = document.getElementById("proyectovigente_ente_financiador");
    var idedovalue = proyectovigente_ente_financiador.value;
*/

    var dpto = new Departamento(); 
    dpto.combobox("distrito_departamento");    




};





Distrito.prototype.combobox_dpto = function( dom, depto ) {     
            
    loader.inicio();
                        
    var url =  html.url.absolute() +'/api/'+this.recurso+'/departamento/'+depto ;    
    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();            
        })
        .then(data => {
            //var data = response.text();
            
            var domobj = document.getElementById(dom);
            //var idedovalue = domobj.value;            

            var i, L = domobj.options.length - 1;
            for(i = L; i >= 0; i--) {
               domobj.remove(i);
            }


            if (!(data == "")){
                //console.log("data vaciooooooooooooooooooo")
     
                var oJson = JSON.parse( data ) ;

                for( x=0; x < oJson.length; x++ ) {

                    var jsonvalue = (oJson[x]['distrito'] );      

                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
//                    }
                }       

            }
            
                //resolve( data );
            loader.fin();
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
*/

}





Distrito.prototype.combobox_dpto_all = function( dom, depto ) {     
            
    loader.inicio();
                        
    var url =  html.url.absolute() +'/api/'+this.recurso+'/departamento/'+depto ;    
    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();            
        })
        .then(data => {
            //var data = response.text();
            
            var domobj = document.getElementById(dom);
            //var idedovalue = domobj.value;            

            var i, L = domobj.options.length - 1;
            for(i = L; i >= 0; i--) {
               domobj.remove(i);
            }

            var opt = document.createElement('option');            
            opt.value = 0;
            opt.innerHTML = "Todos";                        
            domobj.appendChild(opt);  




            if (!(data == "")){
                //console.log("data vaciooooooooooooooooooo")
     
                var oJson = JSON.parse( data ) ;

                for( x=0; x < oJson.length; x++ ) {

                    var jsonvalue = (oJson[x]['distrito'] );      

                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
//                    }
                }       

            }
            

            
            
            
                //resolve( data );
            loader.fin();
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
*/

}

