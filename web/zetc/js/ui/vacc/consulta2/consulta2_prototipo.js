 
 
 function Consulta2(){
    
   this.tipo = "vacunacion";   
   this.recurso = "vacunaciones";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/vacc";   
   
   
   
   this.campoid=  'fecha';
   this.tablacampos =  ['fecha', 'cedula', 'nombre', 'email'];
   
   this.etiquetas =  ['fecha', 'cedula', 'nombre', 'email'];
    
   this.tablaformat = [ 'D', 'N', 'C', 'C' ];                                  
   
   this.tbody_id = "vacunacion-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "vacunacion-acciones";   
   
}









Consulta2.prototype.form_ini = function() {    
  
  

 
  

    
    var registrador = document.getElementById('registrador');   
    registrador.onblur  = function() {      
                  
        registrador.value = fmtNum(registrador.value);      
        registrador.value = NumQP(registrador.value);      
    
        var  id = (registrador.value );
        
        
        ajax.url = html.url.absolute() +'/api/registradores/'+id;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {                               
                    var ojson = JSON.parse(xhr.responseText) ;     
                                      
                    document.getElementById('cedula_registrador').value = ojson['cedula'];
                   
                    document.getElementById('registrador_descripcion_out').innerHTML = 
                            ojson['nombre'];
                    
                    
                }
                else{
                    
                    document.getElementById('cedula_registrador').value = 0;                   
                    document.getElementById('registrador_descripcion_out').innerHTML = "Todos";
                    
                }                                   

            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            });             
             
     };      
    registrador.onblur();       
        
    
    
    var ico_more_registrador = document.getElementById('ico-more-registrador');
    ico_more_registrador.addEventListener('click',
        function(event) {     

            var obj = new Registrador();      
            
            obj.acctionresul = function(id) {    
                registrador.value = id; 
                registrador.onblur(); 
            };       
            
            modal.ancho = 900;
            busqueda.modal.objeto(obj);

        },
        false
    );        
         
    
    
    
    var cedula_registrador = document.getElementById('cedula_registrador');   
    cedula_registrador.onblur  = function() {                        
                  
        cedula_registrador.value = fmtNum(cedula_registrador.value);      
        cedula_registrador.value = NumQP(cedula_registrador.value);      
    
        var  cedu = (cedula_registrador.value );

        ajax.url =  html.url.absolute()+'/api/registradores/cedula/'+cedu;
        ajax.async.get()
            .then(( xhr ) => {

                if (xhr.status == 200)
                {     
                    var ojson = JSON.parse(xhr.responseText) ;     
                    
                    registrador.value = ojson['registrador']; 
                    registrador.onblur();     
                    
                }
                else{
                    
                    registrador.value = 0; 
                    registrador.onblur();                                       
                }                  
                
            })            
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            }); 
    
     };    
    
    
     
    
};






Consulta2.prototype.form_validar = function() {    
    
    return true;
};








Consulta2.prototype.main_list = function(obj) {    


    fetch(  html.url.absolute() + '/vacc/consulta2/htmf/form.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        document.getElementById( "arti_cab" ).innerHTML =  data;    


       this.form_ini();
       

            fetch(  html.url.absolute() + '/vacc/consulta2/htmf/lista.html' )
              .then(response => {
                return response.text();
              })
              .then(data => {
                document.getElementById( "arti_det" ).innerHTML =  data;    
        
        
                document.getElementById( "btn_consultar" ).onclick = function()
                {   
                    obj.detalle_promesa(obj, 1);
                    
                };          
        
                
                

                var btn_xlsx = document.getElementById('btn_xlsx');
                btn_xlsx.addEventListener('click',
                    function(event) {     


                        var url = html.url.absolute()+"/consulta2.xlsx"                                                        
                            +"?fe1="+document.getElementById("qry_fecha_inicio").value
                            +'&fe2='+document.getElementById("qry_fecha_finalizacion").value 
                            +'&reg='+document.getElementById("registrador").value                             
                            ;
                        
                        
                        
                        
                        var xhr =  new XMLHttpRequest();                           
                       

                        xhr.onreadystatechange = function() {
                            if(xhr.readyState === xhr.HEADERS_RECEIVED) {
                                //console.log('Request has started');
                            }
                        } ;                   


                        //ajax.xhr.onload = function (e) {
                        xhr.onload = function (e) {

                            if (xhr.readyState === 4 && xhr.status === 200) {

                                var contenidoEnBlob = xhr.response;
                                var link = document.createElement('a');
                                link.href = (window.URL || window.webkitURL).createObjectURL(contenidoEnBlob);

                                link.download = "noregistradas.xlsx";

                                var clicEvent = new MouseEvent('click', {
                                    'view': window,
                                    'bubbles': true,
                                    'cancelable': true
                                });
                                //Simulamos un clic del usuario
                                //no es necesario agregar el link al DOM.
                                link.dispatchEvent(clicEvent);
                                //link.click();                            
                                //ajax.headers.get();                    

                            }
                            else 
                            {
                                if (xhr.status === 401){
                                    var state = xhr.status;     
                                    arasa.html.url.redirect(state);  
                                }
                                else{
                                    alert(" No es posible acceder al archivo");
                                }

                            }

                        };                    



                        xhr.open("POST", url, true);
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                        var type = "application/json";
                        xhr.setRequestHeader('Content-Type', type);   
                        xhr.setRequestHeader("token", localStorage.getItem('token'));  
                        
                        xhr.responseType = 'blob';
                        xhr.send( null );                       


                    },
                    false
                );    

                
                
              })            


      })            






};





Consulta2.prototype.detalle_promesa = function( obj, page   ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();
            obj.page = page;


            var url = html.url.absolute() + '/api/vacunaciones/consulta2/'                                
                +';fe1='+document.getElementById("qry_fecha_inicio").value 
                +';fe2='+document.getElementById("qry_fecha_finalizacion").value 
                +';reg='+document.getElementById("registrador").value                 
                +'?page='+page;

            var xhr =  new XMLHttpRequest();      


            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;


                    var ojson = JSON.parse( tabla.json ) ;     
                    
                    
                    if (tabla.json != "{}"){
                        tabla.json = JSON.stringify(ojson['datos']) ;                                
                        
                    }
                    

                    tabla.ini(obj);
                    tabla.gene();              
                    tabla.formato(obj);
  
  
                    //paginacion  
  
                    if (tabla.json != "{}"){
                        var json_paginacion = JSON.stringify(ojson['paginacion']) ;     
    
                        arasa.html.paginacion.ini(json_paginacion);

                        document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                                = arasa.html.paginacion.gene();                           
                        
                        
                        obj.paginacion_move(obj);

                        
                    }
                    else{
                        document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                            = "";                           
                    }

                        resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};



Consulta2.prototype.paginacion_move = function( obj ) {    

            
        var listaUL = document.getElementById( obj.tipo + "_paginacion" );
        var uelLI = listaUL.getElementsByTagName('li');


        var pagina = 0;


        for (var i=0 ; i < uelLI.length; i++)
        {
            var lipag = uelLI[i];   

            if (lipag.dataset.pagina == "act"){                                     
                pagina = lipag.firstChild.innerHTML;
            }                    
        }



        for (var i=0 ; i < uelLI.length; i++)
        {
            var datapag = uelLI[i].dataset.pagina;     

            if (!(datapag == "act"  || datapag == "det"  ))
            {
                uelLI[i].addEventListener ( 'click',
                    function() {                                      

                        switch (this.dataset.pagina)
                        {
                           case "sig": 
                                   pagina = parseInt(pagina) +1;
                                   break;                                                                          

                           case "ant":                                     
                                   pagina = parseInt(pagina) -1;
                                   break;

                           default:  
                                   pagina = this.childNodes[0].innerHTML.toString().trim();
                                   break;
                        }
                        pagina = parseInt( pagina , 10);                                                                       

                        
                        //var co = new Consulta1;
                        obj.detalle_promesa(obj, pagina);

                        //tabla.refresh_promise( obj, pagina, busca, fn  ) ;

                    },
                    false
                );                
            }            
        }           

};
