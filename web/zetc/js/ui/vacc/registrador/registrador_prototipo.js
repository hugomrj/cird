

function Registrador(){
    
   this.tipo = "registrador";   
   this.recurso = "registradores";   
   this.value = 0;
   this.json_descrip = "nombre";
   this.form_descrip = "registrador_descripcion_out";
      
   
   this.dom="";
   this.carpeta=  "/vacc";   
   
   
   this.titulosin = "Registrador"
   this.tituloplu = "Registradores"   
      
   
   this.campoid=  'registrador';
   this.tablacampos =  ['registrador', 'cedula', 'nombre', 'email', 'habilitado' ];
   
   this.etiquetas =  ['Registrador', 'Cedula', 'Nombre', 'Correo', 'Habilitado'];                                  
    
   this.tablaformat = ['N', 'C', 'C', 'C', 'C'];                                  
   
   this.tbody_id = "registrador-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "registrador-acciones";   
         
   this.parent = null;
   
   this.combobox = 
            {
                "usuario":{
                   "value":"usuario",
                   "inner":"cuenta"
                }
            };      
   

      
   
   
   
}





Registrador.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);     
    
    document.getElementById('id_sel_habilitado').style.display="block";   
    document.getElementById('id_registrador_habilitado').style.display="none";  
    
    obj.form_accion();
    
};





Registrador.prototype.form_ini = function() {    
    
};






Registrador.prototype.form_validar = function() {    
    

    return true;
};










Registrador.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};







Registrador.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() + '/api/'+this.recurso+'/all' ;    

    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));



    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {


            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            
      
            var oJson = JSON.parse( data ) ;


            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['cordinador'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
        */

}





Registrador.prototype.post_form_id = function( obj  ) {                

    document.getElementById('id_sel_habilitado').style.display="none";   
    document.getElementById('id_registrador_habilitado').style.display="block";   
    
};




Registrador.prototype.form_accion = function() {    
    

    // sino    
    var sel_habilitado = document.getElementById('sel_habilitado');   
    sel_habilitado.onchange  = function() {   
    
//        var dptoid = sel_transporte.value;      
        document.getElementById('registrador_habilitado').value =    
                sel_habilitado.value;
    }    
    
    sel_habilitado.onchange();
        
    
    
};



Registrador.prototype.post_form_id = function( obj  ) {                

    document.getElementById('id_sel_habilitado').style.display="none";   
    document.getElementById('id_registrador_habilitado').style.display="block";   
    
    
};




Registrador.prototype.preedit = function( obj  ) {                

    var id_sel_habilitado = document.getElementById('id_sel_habilitado');  
    id_sel_habilitado.style.display="block";   
    
    var id_registrador_habilitado = document.getElementById('id_registrador_habilitado');  
    id_registrador_habilitado.style.display="none";   
    
    
    var vacunador_habilitado = document.getElementById('registrador_habilitado');  
    if (registrador_habilitado.value == "Si"){
        document.getElementById('sel_habilitado').selectedIndex = "0";    
    }
    if (registrador_habilitado.value == "No"){
        document.getElementById('sel_habilitado').selectedIndex = "1";    
    }
    
    obj.form_accion();
    
};




Registrador.prototype.carga_combos = function( obj  ) {                


    var usuario = new Usuario(); 
    usuario.combobox_rol("registrador_usuario");    




};

