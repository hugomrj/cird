

function EstadoProyecto(){
    
   this.tipo = "estadoproyecto";   
   this.recurso = "estadosproyectos";   
   this.value = 0;
   this.form_descrip = "nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Estado Proyecto"
   this.tituloplu = "Estados Proyectos"   
      
   
   this.campoid=  'estado_proyecto';
   this.tablacampos =  ['estado_proyecto', 'nombre'];
   
   this.etiquetas =  ['Tipo', 'Nombre'];                                  
    
   this.tablaformat = ['N', 'C'];                                  
   
   this.tbody_id = "estadoproyecto-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "estadoproyecto-acciones";   
         
   this.parent = null;
   
   
}





EstadoProyecto.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





EstadoProyecto.prototype.form_ini = function() {    
    
};






EstadoProyecto.prototype.form_validar = function() {    
    
   
    var nombre = document.getElementById('estadoproyecto_nombre');    
    if (nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        nombre.focus();
        nombre.select();        
        return false;
    }              
    
    return true;
};










EstadoProyecto.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};






EstadoProyecto.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.absolute() +'/api/'+this.recurso+'/all' ;    

    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

 


    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {


            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            
      
            var oJson = JSON.parse( data ) ;


            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['estado_proyecto'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
        */

}




