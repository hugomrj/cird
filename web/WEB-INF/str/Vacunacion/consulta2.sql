
select t1.fecha, cedula, t1.nombre, t1.email, t1.registrador  
from 
( 
select * from 
( 
SELECT date_trunc('day', dd):: date as fecha 
FROM generate_series 
        ( 'v0'::timestamp , 'v1'::timestamp, '1 day'::interval) dd  
) as mat, vacc.registradores  
) as t1 left join vacc.vacunaciones v on 
(t1.fecha = v.fecha_reporte and t1.registrador = v.registrador) 
where v.registrador is null 
and t1.registrador  v2 
order by fecha, t1.cedula  


