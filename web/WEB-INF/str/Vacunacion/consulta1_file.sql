
SELECT id as codigo, fecha_reporte, registradores.cedula registrador_cedula,  
registradores.nombre registrador_nombre, registradores.email registrador_email, 
vacunadores.cedula vacunador_cedula, vacunadores.nombre vacunador_nombre, 
departamentos.nombre departamento, distritos.nombre distrito,  
cantidad_50_mas, cantidad_18_49, cantidad_18_menos,   
cantidad_vacunados_total,    
transporte, observacion   
FROM vacc.vacunaciones inner join vacc.registradores   
on (vacunaciones.registrador = registradores.registrador) 
inner join vacc.vacunadores  on (vacunaciones.vacunador = vacunadores.vacunador) 
inner join vacc.departamentos on (vacunaciones.departamento = departamentos.departamento) 
inner join vacc.distritos on (vacunaciones.distrito = distritos.distrito)  
where vacunaciones.departamento v0 
and vacunaciones.distrito v1  
and fecha_reporte between 'v2'  and 'v3'      
and vacunaciones.registrador v4    
and vacunaciones.vacunador v5    
order by fecha_reporte, departamento, distrito, vacunaciones.registrador,  
vacunaciones.vacunador   





